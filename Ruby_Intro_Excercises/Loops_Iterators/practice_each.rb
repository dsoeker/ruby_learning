names = ['Bob', 'Joe', 'Steve', 'Janice', 'Susan', 'Helen']

# names.each { |name| puts name }

x = 1

names.each do |name|
  puts "#{x}. #{name}"
  x += 1
end


# Called the .each method on our array. What this method does is to loop through each element in the array. Then begins executing the loop within the block '{}' (The block's starting and ending points). Each time we iterate over the array, we need to assign the value of the element to a variable. In this example we have name the variable 'name' and place it in between two pipes '|'. After that, we write the logic that we want to use to operate on the variable, which represents the current array element. In this case it is simply printing to the screen using 'puts'.
