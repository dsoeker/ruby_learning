loop do
  puts "Do you want to do that again?"
  answer = gets.chomp
  if answer != 'Y' # For every answer that isn't 'Y', break the loop and end the program; otherwise, this will keep asking the same question forever.
    break
  end
end
