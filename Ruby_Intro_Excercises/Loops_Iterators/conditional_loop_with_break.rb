x = 0

while x <= 10
  if x == 7 # Keep looping until reaching 7, then stop the looping
    break
  elsif x.odd?
    puts x
  end
  x += 1
end
