x = [1, 2, 3, 4, 5]

x.each do |a|
  a + 1
end

# Will print each value in the array and add 1 to that value, but it's not really doing anything, so the original array is returned.
