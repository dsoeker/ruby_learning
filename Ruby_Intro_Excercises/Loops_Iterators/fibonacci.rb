def fibonacci(number)
  if number < 2
    number
  else
    fibonacci(number - 1) + fibonacci(number - 2)
  end
end

for i in 1..20 do
  puts fibonacci(i)
end
