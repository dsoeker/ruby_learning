# Gets a number from the user and counts down to 0. Once 0 is reached, the program ends and puts "Done!" to the screen.

x = gets.chomp.to_i

while x >= 0
  puts x
  x = x - 1
  # Can also be written as:
  # x -= 1
  # A way to modify the variable in some way must be entered; otherwise, this will cause an infinite loop.
  
end

puts "Done!"
