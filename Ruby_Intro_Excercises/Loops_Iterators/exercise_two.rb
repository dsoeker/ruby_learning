puts "Enter a number and I'll read it back: "
user_input = gets.chomp.to_i

counter = 1
while counter <= 10
  puts "#{counter}. #{user_input}"
  puts "Want more?"
  new_input = gets.chomp.upcase
  if new_input == "STOP"
    break
  else
    counter += 1
  end
end

puts "Done! Go away."
