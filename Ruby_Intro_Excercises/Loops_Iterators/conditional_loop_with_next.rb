x = 0

while x <= 10
  if x == 3 # This will skip the number 3 and continue with the looping
    x += 1
    next
  elsif x.odd?
    puts x
  end
  x += 1
end
