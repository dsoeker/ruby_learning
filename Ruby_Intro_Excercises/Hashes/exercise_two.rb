# Look at Ruby's merge method. Notice that it has two versions. What is the difference between merge and merge!? Write a program that uses both and illustrate the differences.

# Merge by itself will not mutate the caller. However, when you add ! to the end, any changes made will be permanent.

phone_extensions = {'Steven' => 34523, 'George'=> 32123, 'John'=> 80984}
new_extensions = {'Steven'=> 34523, 'George'=> 87656, 'John'=> 80984}

puts "This will show the temporary change to the array from the merge method:"
puts phone_extensions.merge(new_extensions)
puts phone_extensions
puts "=" * 20
puts "This will show how the caller was mutated:"
phone_extensions.merge!(new_extensions)
puts phone_extensions
