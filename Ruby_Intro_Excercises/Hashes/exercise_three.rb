# Using some of Ruby's built-in Hash methods, write a program that loops through a hash and prints all of the keys. Then write a program that does the same thing except printing the values. Finally, write a program that prints both.

phone_extensions = {'Steven' => 34523, 'George'=> 32123, 'John'=> 80984}

puts "Names:"
phone_extensions.select { |name, extension|
  puts name
}

puts "Extensions:"
phone_extensions.select { |name, extension|
  puts extension
}

puts "Key-value Pairs:"
phone_extensions.select { |name, extension|
  puts "#{name}: #{extension}"
}
