# What method could you use to find out if a Hash contains a specific value in it? Write a program to demonstrate this use.

# Use the has_value? method.

person = {name: 'Bob', occupation: 'web developer', hobbies: 'painting'}

if person.has_value? "Bob"
  puts "The Hash has the name: Bob"
else
  puts "Bob not found in the Hash."
end
