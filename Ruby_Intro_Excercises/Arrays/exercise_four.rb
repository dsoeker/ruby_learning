# What does each method return in the following example?

arr = [15, 7, 18, 5, 12, 8, 5, 1]

puts arr.index(5)
# Returns the index of value 5

puts arr.index[5]
# Returns Enumerator

puts arr[5]
# Returns the value at index 5
