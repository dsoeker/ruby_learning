=begin
1. arr = ["b", "a"]
   arr = arr.product(Array(1..3))
   arr.first.delete(arr.first.last)
   # [[b, 1], [b, 2], [b, 3], [a, 1], [a, 2], [a, 3]]
   # returns 1
   # [[b], [b, 2], [b, 3], [a, 1], [a, 2], [a, 3]]
=end
arr = ["b", "a"]
   puts arr = arr.product([Array(1..3)]) # Using the [] around the Array function will nest a new array inside the existing
   arr.first.delete(arr.first.last)
   puts arr
   # [[b, [1, 2, 3]], [a, [1, 2, 3]]]
   # [[b], [a, [1, 2, 3]]]
