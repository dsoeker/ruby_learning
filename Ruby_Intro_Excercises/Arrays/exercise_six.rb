names = ['bob', 'joe', 'susan', 'margaret']
names['margaret'] = 'jody'

TypeError: no implicit conversion of String into Integer
  from (irb):2:in '[]='
  from (irb):2
  from /Users/username/.rvm/rubies/ruby-2.0.0-p353/bin/irb:12:in '<main>'

# The above error is a result of the names['margaret'] = 'jody' line. This isn't calling or doing anything because the item being called in th [] is a string. If you want to remove the last item and replace it with something else, use: names.pop('margaret'), names.push('jody'), or names[3] = 'jody'
