# 12. Given the following data structures. Write a program that moves the information from the array into the empty hash that applies to the correct person.

contact_data = [["joe@email.com", "123 Main st.", "555-123-4567"],
            ["sally@email.com", "404 Not Found Dr.", "123-234-3454"]]

contacts = {"Joe Smith" => {}, "Sally Johnson" => {}}

key_array = [:email, :address, :phone]
nest_count = 0
key_array.select { |key_value|
  contacts["Joe Smith"][key_value] = contact_data[0][nest_count]
  contacts["Sally Johnson"][key_value] = contact_data[1][nest_count]
  nest_count += 1
}

=begin
contacts.each do |name, hash|
  fields.each do |field|
    hash[field] = contact_data.shift
  end
end

contacts.each_with_index do |(name, hash), idx|
  fields.each do |field|
    hash[field] = contact_data[idx].shift
  end
end
=end

puts contacts
puts "==" * 20

# 13.Using the hash you created from the previous exercise, demonstrate how you would access Joe's email and Sally's phone number?

puts "Joe's phone: #{contacts["Joe Smith"][:phone]}"
puts "Sally's phone: #{contacts["Sally Johnson"][:phone]}"
