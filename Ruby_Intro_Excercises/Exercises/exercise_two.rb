# Using the same array as exercise one, print out the values, but only those greater than 5.

number_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

number_array.each do |number|
  if number > 5
    puts number
  end
end

# one-line version: arr.each { |number| puts number if number > 5 }
