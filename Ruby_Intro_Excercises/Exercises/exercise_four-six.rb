# 4. Append "11" to the end of the original array. Prepend "0" to the beginning.

number_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
number_array.push(11)
number_array.unshift(0)

p "4. #{number_array}"
puts "--" * 20

# 5. Get rid of "11" and append "3".
number_array.pop
number_array.push(3)
p "5. #{number_array}"
puts "--" * 20

# 6. Get rid of duplicates without specifically removing any one value.
number_array.uniq!
p "6. #{number_array}"
