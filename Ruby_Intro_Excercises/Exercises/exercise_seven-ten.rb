# 7. What's the major difference between an Array and a Hash?

puts "7. A hash is a data structure that stores item by associated keys. This is contrasted against array, which stores items by an ordered index."
puts '--' * 20

# 8. Create a Hash using both Ruby syntax styles.
example_hash_old = {:name => "Dustin", :age => 28}
example_hash_new = {name: "Dustin", age: 28}

# 9. Suppose you have a hash:
h = {a:1, b:2, c:3, d:4}

  # 1. Get the value of key `:b`.
    h[:b]

  #2. Add to this hash the key:value pair `{e:5}`
    h[:e] = 5

  #3. Remove all key:value pairs whose value is less than 3.5
    h.delete_if do |key, value|
      value < 3.5
    end

# 10. Can hash values be arrays? Can you have an array of hashes? (give examples)

hash_example = [{a: 1, b: 2, c: 3}, {d: 4, e: 5, f: [6, 7, 8]}]
