zero = 0
puts "Before each call"
zero.each { |element| puts element } rescue puts "Can't do that!"
puts "After each call"

# The reason it prints 'Can't do that' is it isn't possible to call the each method on an intege which is the value of the zero variable.
