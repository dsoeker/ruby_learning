# What will the following program print to the screen? What will it return?

def execute(&block)
  block.call
end

execute { puts "Hello from inside the execute method!" }

# It will return a proc object, but print nothing since there is no call method in the block in the execute method.

# Modify the code in exercise 2 to make the block execute properly. Added the '.call' method on line 4.

# If the '&' was removed when the argument was passed in, then an 'execute' error would be thrown.
