# Rewrite your program from exercise 3 using a case statement. Wrap the statement from exercise 3 in a method and wrap this new case statement in a method. Make sure they both still work.

def number_checker (number)
  case number
  when 0..50
    "#{number} is between 0 and 50"
  when 51..100
    "#{number} is between 51 and 100"
  else
    "#{number} is above 100"
  end
end

puts "Enter number: "
number = gets.chomp.to_i

puts number_checker(number)
