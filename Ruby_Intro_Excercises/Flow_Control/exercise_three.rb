# Write a program that takes a number from the user between 0 and 100 and reports back whether the number is between 0 and 50, 51 and 100, or above 100.

def number_checker (number)
  if number < 0
    "#{number} less than 0"
  elsif number >= 0 && number <= 50
    "#{number} is between 0 and 50"
  elsif number >= 51 && number <= 100
    "#{number} is between 51 and 100"
  elsif number > 100
    "#{number} is above 100"
  end
end

puts "Enter number: "
number = gets.chomp.to_i

puts number_checker(number)
