#Write a method that takes a string as argument. The method should return the all-caps version of the string, only if the string is longer than 10 characters. Example: change "hello world" to "HELLO WORLD". (Hint: Ruby's String class has a few methods that would be helpful. Check the Ruby Docs!)

def string_alteration (string_value)
  if string_value.length > 10
    string_alteration = string_value.upcase
  else
    string_alteration = string_value
  end
end

puts string_alteration("hello")
