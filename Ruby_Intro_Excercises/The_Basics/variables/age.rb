# Write a program called age.rb that asks a user how old they 
# are and then tells them how old they will be in 
# 10, 20, 30 and 40 years. Below is the output for someone 
# 20 years old.

age_range = [10, 20, 30, 40]

puts "How old are you?"
age = gets.chomp.to_i

age_range.each do |year|
  puts "In #{year} years you will be: #{age + year}"
end 



