
# Program 1
x = 0
3.times do
  x += 1
end
puts x

# Program 2
y = 0
3.times do
  y += 1
  x = y
end
puts x

# What does x print to the screen in each case? 
  # 3, undefined local variable

# Do they both give errors? 
  # Only 1
  
# Are the errors different? Why?
  # Only the second program should throw an error. The variable
  # x, is only available inside the scope of the block.
