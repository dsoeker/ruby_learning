# Use the dates from the previous example and store them in an array. Then make your program output the same thing as exercise 3.

movie_info = {Star_Wars: 1976,
              Star_Trek: 1978,
              Transformers: 2007,
              The_Avengers: 2012,
              Ant_Man: 2015
             }

movie_array = []

movie_info.each do |title, year|
  movie_array << year
end

puts movie_array
