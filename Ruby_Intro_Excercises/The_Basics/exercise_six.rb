# Write a program that calculates the squares of 3 float numbers
# of your choosing and outputs the result to the screen.

number_array = [4.3, 6.13, 124.34]
number_array.each do |number|
  puts (number ** 2).round(2)
end
