# Write a program that uses a hash to store a list of movie titles with the year they came out.
# Then use the puts command to make your program print out the year of each movie to the screen.

movie_info = {Star_Wars: 1976,
              Star_Trek: 1978,
              Transformers: 2007,
              The_Avengers: 2012,
              Ant_Man: 2015
              }

movie_info.each do |title, year|
  puts year
end
