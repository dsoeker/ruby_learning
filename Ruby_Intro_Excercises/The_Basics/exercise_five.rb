# Write a program that outputs the factorial of the numbers 5, 6, 7, and 8.

def factorial(number)
  if number == 0
    return 1
  else
    return number * factorial(number - 1)
  end
end

number_array = [5, 6, 7, 8]

number_array.each do |factorial_count|
  puts factorial(factorial_count)
end
