# What will the following code output?

# Nothing. The last return statement 
# has no value associated with it. 
# Thus, it will ignore the puts statement that follows.

def scream(words)
  words = words + "!!!!"
  return
  puts words
end

scream("Yippeee")
