# What do the following expressions evaluate to?

x = 2 # Stored variable, won't do anything yet.

puts x = 2 # Prints out the value 2, but returns nil.

p name = "Joe" # Prints the value of name, and returns "Joe" as well.

four = "four" # Samething as number 1. Will store the string "four".

print something = "nothing" # Prints the value of 'something', "nothing". 
                            # This will not add a new line to the end of the
                            # output like puts will.
                            
                            


