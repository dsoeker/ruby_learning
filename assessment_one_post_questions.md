## Mutations
----

**Comment on Question 3** The param was reassigned as before it pointed to the "hello" string object. (-1)

```ruby
def change(param)
  param = "hi"
  param << " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Response:**
My understanding was the variable ```param``` was reassigned: "the first line ```param = "hi"``` initializes and assigns the value of ```param``` to something other than what was passed into the method, now making this pass by value." I guess this sentence should have been more clear.

----

**Comment on Question 4** "Now. however, there's a new local variable in the method param assigned to value "hellogreeting".. There is no new local variable, param was there before. In the second point, the language used is not correct. We are not concatenating (as concatenating would produce new string object, here we did not) and the param = "hellogreetinghey" would mean reassignment, String#<< does not reassign, but mutates the original String object. (-4)

```ruby
def change(param)
  param += "greeting"
  param << "hey"
  param = "hi"
  param << " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Response:**
Within the ```change``` method;

  1. ```param += "greeting"``` > Takes the initial object, ```"hello"```, passed into the ```change``` method and appends ```greeting```, thus creating a new object, ```hellogreeting```. This turns focus away from the original object and makes this pass-by-value.

  2. ```param << "hey"``` > Takes the String object ```"hellogreeting"``` and appends ```"hey"``` mutating the ```"hellogreeting"``` object to ```"hellogreetinghey"```.

  3. ```param = "hi"``` > Reassigns the ```param``` variable to ```"hi"``` instead of ```"hellogreetinghey"```.

  4. ```param << "world"``` > This mutates the ```"hi"``` object to```"hi world"```.

^ I hope this what you were looking for.

## Collections
----

**Comment on Question 5** "since map is designed to return a new collection for each element in current object, whether true or false" - this part of the answer is confusing, whether it is true or false, it does not matter. What matters in case of the map method? (-3)

**Response:**
It really doesn't matter what's inside the block for ```map```; however, if no block is given, then an enumerator is returned. The comparison I was making here was ```select``` will only return a new object with the items that returned true in the original block, while ```map``` will return all items but will allow you to modify them in some way.

----

**Comment on Question 6** "n + 1" will be non-nil and non-false too, so your explanation is not correct. n + 1 will always be a number, which is truthy. (-5)

**Response:**
While "truthy", having the expression ```n + 2``` in the block of the ```select``` method is not necessarily asking for certain items to be selected or pulled out from the collection. However, since ```n + 2``` is "truthy" and will return an integer, the method did return the collection ```[1, 2, 3]```.  

----

**Comment on Question 7** When you run the code in irb, you wont see [1,2,3] array printed, but 1, 2, 3 each on a new line.

**Response:**
True, when you run the code in irb 1, 2, and 3 appear on their own lines and an empty array is returned.
