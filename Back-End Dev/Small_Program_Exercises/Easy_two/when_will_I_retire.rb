# Build a program that displays when the user will retire and how many years she has to work till retirement.

# Example:
#------------

# What is your age? 30
# At what age would you like to retire? 70

# It's 2016. You will retire in 2056.
# You have only 40 years of work to go!

def year_diff(current_age, retire_age)
  diff = retire_age - current_age
  puts "You only have #{diff} years of work to go!"
end

def year_find(current_age, retire_age)
  current_year = Time.now.year
  retire_year = (retire_age - current_age) + current_year
  puts "It's #{current_year}. You will retire in #{retire_year}."
  year_diff(current_age, retire_age)
end 

puts 'What is your age?'
current_age = gets.chomp.to_i

puts 'At what age would you like to retire?'
retire_age = gets.chomp.to_i

year_find(current_age, retire_age)
