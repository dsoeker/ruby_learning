# What will the following code print, and why? Don't run the code until you have tried to answer.

array1 = %w(Moe Larry Curly Chemp Harpo Chico Groucho Zeppo)
array2 = []
array1.each { |value| array2 << value }
array1.each { |value| value.upcase! if value.start_with?('C') }
puts array2

# For an array, using the << operator is the same as pointing a variable to a string. Now array1 and array2 point to the same reference. Regardless, of what happens inside the loop statements, it will affect both variables. This is another pass-by-reference problem.

# ---

# Wait a minute! We changed Curly to CURLY, 'Chemp' to 'CHEMP', and Chico to CHICO in array1. How'd those changes end up in array2 as well?

# The answer lies in the fact that the first each loop simply copies a bunch of references from array1 to array2. When that first loop completes, both arrays not only contain the same values, they contain the same String objects. If you modify one of those Strings, that modification will show up in both Arrays.