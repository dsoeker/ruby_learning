# Build a program that asks a user for the length and width of a room in meters and then displays the area of the room in both square meters and square feet.

# Note: 1 square meter == 10.7639 square feet

# Do not worry about validating the input at this time.

# Example Output: 
#-----------------------
# Enter the length of the room in meters:
# 10
# Enter the width of the room in meters:
# 7
# The area of the room is 70.0 square meters (753.47 square feet).


def conversions(length, width, scale)
  if scale == "feet"
    (length * width).round(2) 
  elsif scale == "inch"
    ((length * width) / 12).round(2) 
  elsif scale == "centimeter"
    ((length * width) * 30.48).round(2)
  else
    "[Conversion not found]"
  end
end

puts 'Enter the length of the room in feet: '
length = gets.chomp.to_i

puts 'Enter the width of the room in feet: '
width = gets.chomp.to_i

sq_feet = conversions(length, width, "feet")
sq_inch = conversions(length, width, "inch")
sq_centimeters = conversions(length, width, "centimeter")

puts "The area of the room is #{sq_feet} square feet."
puts "Which is: #{sq_inch} square inches."
puts "Which is: #{sq_centimeters} square centimeters."

