# Write a program that will ask for user's name. The program will then greet the user. If the user writes "name!" than the computer yells back to the user.

# Examples
#-------------

# What is your name? Bob
# Hello Bob.
# What is your name? Bob!
# HELLO BOB. WHY ARE WE SCREAMING?

def greeting(name)
  if name.include? "!"
    puts "HELLO, #{name.upcase.chop}. WHY ARE WE SCREAMING?"
  else
    puts "Hello, #{name}."
  end
end

def get_name
  puts "What is your name?"
  name = gets.chomp
  greeting(name)
end

get_name

# String#chop or String#[]= are very useful in reducing the size of string.