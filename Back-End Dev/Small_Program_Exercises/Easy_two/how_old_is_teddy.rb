# Build a program that randomly generates and prints Teddy's age. To get the age, you should generate a random number between 20 and 200.

# Output: Teddy is 69 years old!

def teddys_age
  rand(20..200)  
end

puts 'What is your name:'
name = gets.chomp
name = "Teddy" if name.empty?

puts "#{name} is #{teddys_age} years old!"

