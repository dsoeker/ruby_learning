# Write a program that asks the user to enter an integer greater than 0, then asks if the user wants to determine the sum or product of all numbers between 1 and the entered integer.

# Examples:
# --------------
# >> Please enter an integer greater than 0:
# 5
# >> Enter 's' to compute the sum, 'p' to compute the product.
# s
# The sum of the integers between 1 and 5 is 15.


# >> Please enter an integer greater than 0:
# 6
# >> Enter 's' to compute the sum, 'p' to compute the product.
# p
# The product of the integers between 1 and 6 is 720.

def prompt(message)
  puts ">> #{message}"
end

def perform_operation(number, operation)
  if operation == 's'
    final_total = (1..number).inject(0, :+)
  elsif operation == 'p'
    final_total = (1..number).inject(1, :*)
  end
  op_val = 'product' if operation == 'p'
  op_val = 'sum' if operation == 's'
  puts "The #{op_val} of the integers between 1 and #{number} is #{final_total}."
end

def get_operation(number)
  operation = ''
  loop do
    prompt "Enter 's' to compute the sum, 'p' to compute the product."
    operation = gets.chomp.downcase
    break if %w(s p).include?(operation)
    prompt 'Invalid entry, try again.'
  end
  perform_operation(number, operation)
end

def get_number
  number = ''
  loop do
    prompt 'Please enter an integer greater than 0:'
    number = gets.chomp.to_i
    break if number > 0
    prompt 'Invalid entry, try again.'
  end
  get_operation(number)
end

get_number


# So, what does the inject method accomplish? Combines all of the elements of a enum by applying a binary operation. Specified by a block or a symbol.