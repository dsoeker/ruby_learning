# Create a simple tip calculator. The program should prompt for a bill amount and a tip rate. The program must compute the tip and then display both the tip and the total amount of the bill.

#What is the bill? 200
#What is the tip percentage? 15

#The tip is $30.0
#The total is $230.0

def calc_total(amount, tip)
  total = (amount + tip).round(2)
  puts "The total is $%.2f" % total
end

def calc_tip(amount, percent)
  tip = ((percent / 100.0) * amount).round(2)
  puts "The tip is $%.2f" % tip
  calc_total(amount, tip)
end

puts 'What is the bill?'
amount = gets.chomp.to_f

puts 'What is the tip percentage?'
percent = gets.chomp.to_f

calc_tip(amount, percent)

