# Print the even numbers from 1 to 99, inclusive. All numbers should be printed on separate lines.

def even_numbers
  new_arr = []
  (1..99).each { |n| new_arr << n if n % 2 == 0 }
  puts new_arr
end

even_numbers
