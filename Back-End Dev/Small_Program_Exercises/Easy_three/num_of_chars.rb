# Write a program that will ask a user for an input of a word or multiple words and give back the number of characters. Spaces should not be counted as a character.

# input:

# Please write word or multiple words: walk
# output:

# There are 4 characters in "walk".
# input:

# Please write word or multiple words: walk, don't run
# output:

# There are 13 characters in "walk, don't run".

def check_string(user_string)
  count_char = user_string.gsub(/\s+/, "")
  puts "There are #{count_char.length} characters in: \"#{user_string}\"."
end

puts 'Please write a word or multiple words: '
user_string = gets.chomp
check_string(user_string)

# .delete(' ').size < This is also an option for remove white space in a string.