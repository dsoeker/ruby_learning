# Create a method that takes two arguments, multiplies them together, and returns the result.

# Example:
# -----------
# multiply(5.2, 3) == 15.6


def multiply(num_one, num_two)
  num_one * num_two
end

puts multiply(5.2, 3)

# It's important to remember to focus on the method's return value and not its output. Keep in mind that puts returns nil.
