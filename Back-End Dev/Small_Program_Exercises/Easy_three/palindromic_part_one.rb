# Write a method that returns true if the string passed as an argument is a palindrome, false otherwise. (A palindrome reads the same forwards and backwards.) Case matters, and all characters should be considered.

# Examples:
# ---------------
# palindrome?('madam') == true
# palindrome?('Madam') == false          # (case matters)
# palindrome?("madam i'm adam") == false # (all characters matter)
# palindrome?('356653') == true

def palindrome?(string)
  string == string.reverse
end

def palindrome_arr?(array)
  arr_to_str = array.join("")
  arr_to_str == arr_to_str.reverse
end

puts palindrome?("madam i'm adam")
puts palindrome_arr?([1, 2, 3, 4, 5])
puts palindrome_arr?(['a', 'b', 'b', 'a'])
puts palindrome_arr?(['cheese'])