# Write a program that solicits 6 numbers from the user, then prints a message that describes whether or not the 6th number appears amongs the first 5 numbers.

# Examples:
# ------------
# ==> Enter the 1st number:
# 25
# ==> Enter the 2nd number:
# 15
# ==> Enter the 3rd number:
# 20
# ==> Enter the 4th number:
# 17
# ==> Enter the 5th number:
# 23
# ==> Enter the last number:
# 17
# The number 17 appears in [25, 15, 20, 17, 23].

# ==> Enter the 1st number:
# 25
# ==> Enter the 2nd number:
# 15
# ==> Enter the 3rd number:
# 20
# ==> Enter the 4th number:
# 17
# ==> Enter the 5th number:
# 23
# ==> Enter the last number:
# 18
# The number 18 does not appear in [25, 15, 20, 17, 23].

NUM_ARR = []

def prompt(message)
  puts "==> #{message}"
end 

def test_last_number
  last_number = NUM_ARR.pop
  if NUM_ARR.include? last_number
    puts "The number #{last_number} appears in #{NUM_ARR}."
  else
    puts "The number #{last_number} does not appear in #{NUM_ARR}."
  end
end 

def enter_number
  message_arr = %w(1st 2nd 3rd 4th 5th last)
  message_arr.each do |msg_val|
    prompt "Enter the #{msg_val} number:"
    number = gets.chomp.to_i
    NUM_ARR << number
  end
  test_last_number
end

enter_number
