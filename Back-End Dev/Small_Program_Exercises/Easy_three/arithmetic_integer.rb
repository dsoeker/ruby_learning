# Write a program that prompts the user for two positive integers, and then prints the results of the following operations on those two numbers: addition, subtraction, product, quotient, remainder, and power. Do not worry about validating the input.

# Example
# -----------
# ==> Enter the first number:
# 23
# ==> Enter the second number:
# 17
# ==> 23 + 17 = 40
# ==> 23 - 17 = 6
# ==> 23 * 17 = 391
# ==> 23 / 17 = 1
# ==> 23 % 17 = 6
# ==> 23 ** 17 = 141050039560662968926103

NUMBERS = []

def prompt(message)
  puts "==> #{message}"
end

def gets_inputs
  num_vals = %w(first second)
  num_vals.each do |val|
    prompt "Enter the #{val} number:"
    number = gets.chomp.to_i
    NUMBERS << number
  end
end

gets_inputs

prompt("#{NUMBERS[0]} + #{NUMBERS[1]} = #{NUMBERS[0] + NUMBERS[1]}")
prompt("#{NUMBERS[0]} - #{NUMBERS[1]} = #{NUMBERS[0] - NUMBERS[1]}")
prompt("#{NUMBERS[0]} * #{NUMBERS[1]} = #{NUMBERS[0] * NUMBERS[1]}")
prompt("#{NUMBERS[0]} / #{NUMBERS[1]} = #{NUMBERS[0] / NUMBERS[1]}")
prompt("#{NUMBERS[0]} ** #{NUMBERS[1]} = #{NUMBERS[0] ** NUMBERS[1]}")