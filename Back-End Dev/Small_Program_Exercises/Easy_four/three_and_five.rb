# Write a method that computes the sum of all numbers between 1 and some other number that are a multiple of 3 or 5. For instance, if the supplied number is 20, the result should be 98 (3 + 5 + 6 + 9 + 10 + 12 + 15 + 18 + 20).

# You may assume that the number passed in is an integer greater than 1.

# Examples

# multisum(3) == 3
# multisum(5) == 8
# multisum(10) == 33
# multisum(1000) == 234168

def multisum(number)
  num_arr = []
  (1..number).each do |num|
    num_arr << num if num % 3 == 0 || num % 5 == 0
  end
  num_arr.inject(0){|sum,x| sum + x }
end

puts multisum(3)
puts multisum(5)
puts multisum(10)
puts multisum(1000)