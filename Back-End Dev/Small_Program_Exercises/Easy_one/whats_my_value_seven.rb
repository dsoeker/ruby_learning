a = 7
array = [1, 2, 3]

array.each do |element|
  a = element
end

puts a

# The each block still has the visibility of local variables that aren't defined inside the block. This block will iterate over the collection assigned to 'array' and assigns each one of the value in the squence until finally ending with 3.

#--

# We are now dealing with blocks; the rules for blocks differ from the rules for methods. In methods, local variables are self-contained. Not so in blocks; blocks can use and modify local variables that are defined outside the block.

# In this case, a starts out as 7, then we set a to 1, 2 and 3 in sequence. By the time we get to the puts, a has a value of 3.