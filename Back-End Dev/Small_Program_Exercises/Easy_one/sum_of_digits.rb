=begin
 Write a method that takes one argument, a positive integer, and returns the sum of its digits.

Examples:

puts sum(23) == 5
puts sum(496) == 19
puts sum(123_456_789) == 45
The tests above should print true.

For a challenge, try writing this without any basic looping constructs (while, until, loop, and each).
=end

def sum(int)
  count = 0
  int.to_s.split('').each { |num| count += num.to_i }
  return count
end

puts sum(23)

def sum(number)
  number.to_s.chars.map(&:to_i).reduce(:+)
end

23.to_s        # => "23"
  .chars       # => ["2", "3"]
  .map(&:to_i) # => [2, 3]
  .reduce(:+)  # => 5