# Write a method that counts the number of occurrences of each element in a given array.

vehicles = ['car', 'car', 'truck', 'car', 'SUV', 'truck', 'motorcycle', 'motorcycle', 'car', 'truck']

# count_occurrences(vehicles)
# Once counted, print each element alongside the number of occurrences.

# Expected output:

# car => 4
# truck => 3
# SUV => 1
# motorcycle => 2

def count_occurrences(vehicles)
  counts = Hash.new(0)
  vehicles.each { |name| counts[name] += 1 }
  counts.each { |k, v| puts "#{k} => #{v}" }
end

count_occurrences(vehicles)

=begin
Other possibility
def count_occurrences(array)
  occurrences = {}

  array.uniq.each do |element|
    occurrences[element] = array.count(element)
  end

  occurrences.each do |element, count|
    puts "#{element} => #{count}"
  end
end

The first task is to iterate over the given array and add each element to an empty hash. The tricky part here is the duplicate values in the array. We don't want to count elements of the same type more than once. To remedy this, we use Array#uniq before we invoke Array#each on array. #uniq simply removes the duplicate values in the given array. This way, we know every key added to the hash will be unique.

As we iterate over each element, we create a new key-value pair in occurrences, with the key as the element's value. To count each occurrence, we use Array#count to count the number of duplicates in the array. But wait, didn't we already remove the duplicates with #uniq? No, because we invoked #uniq in the same statement as #each, therefore, array wasn't mutated.

Lastly, to print the desired output, we call #each on the newly created occurrences, which lets us pass the keys and values as block parameters. Then, inside of the block, we invoke #puts to print each key-value pair.
=end