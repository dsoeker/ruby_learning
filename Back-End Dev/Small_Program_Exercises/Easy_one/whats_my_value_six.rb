a = 7

def my_value(b)
  b = a + a
end

my_value(a)
puts a

# This will result in an error. The inner scope of the my_value method, has two variables, 'a' and 'b'. However, only 'b' has been defined leaving an empty assignment to the 'a' inside the method (seperate from the 'a' at the beginning of the program).

#--

# Even though a is defined before my_value is defined, it is not visible inside my_value. Methods are self contained with respect to local variables; local variables defined inside the method are not visible outside the method, and local variables defined outside the method are not visible inside the method.

# Note, however, that local variables will be visible (via closures) inside blocks, procs, and lambdas.
