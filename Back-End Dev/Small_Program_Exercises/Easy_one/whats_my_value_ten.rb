a = 7
array = [1, 2, 3]

def my_value(ary)
  ary.each do |b|
    a += b
  end
end

puts my_value(array)
puts a

# The 'a' inside the method has inner scope, and cannot be affected by the 'a' with outer scope. Regardless, the '+' method is invalid.

#--

# a at the top level is not visible inside the block because the block is inside my_value, and methods are self-contained with respect to local variables. Since the outer a is not visible inside my_value, it isn't visible inside the block.