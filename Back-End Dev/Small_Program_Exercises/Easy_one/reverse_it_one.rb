=begin
Write a method that takes one argument, a string, and returns the same string with the words in reverse order.

Examples:

puts reverse_sentence('') == ''
puts reverse_sentence('Hello World') == 'World Hello'
puts reverse_sentence('Reverse these words') == 'words these Reverse'
The tests above should print true.
=end

def reverse_sentence(str)
  return '' if str.empty?
  new_str = str.split(/\W+/).reverse
  return new_str.join(' ')
end

puts reverse_sentence('Reverse these words')


def reverse_sentence(string)
  string.split.reverse.join(' ')
end