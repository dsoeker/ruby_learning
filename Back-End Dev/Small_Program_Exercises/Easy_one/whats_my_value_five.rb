a = "Xyzzy"

def my_value(b)
  b = 'yzzyX'
end

my_value(a)
puts a

# The method my_value passes the parameter 'b', intializes it, and points a new value to it. This turns the method into pass-by-value. Thus, the method returns 'yzzyX', and the original variable a still points to "Xyzzy".

#--

# This problem is nearly identical to the previous problem, except this time we are assigning directly to the variable b. Rather than modifying the string like b[2] = '-' does, assigning to b merely changes the String that is referenced by b, not the String itself. As a result, a is not modified.

# Another way to look at this is that assignment to a variable never mutates the object that is referenced. However, don't take that too far: if you assign to b[2] like we did in the previous exercise, that's a completely different operation; that actually mutates the content of the String referenced by b.