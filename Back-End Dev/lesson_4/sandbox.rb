require 'yaml'
MESSAGES = YAML.load_file('twenty_one_messages.yml')

CARD_VALUES = %w(2 3 4 5 6 7 8 9 10 Jack Queen King Ace).freeze
CARD_SUITS = %w(Clubs Diamonds Hearts Spades).freeze
MAXIMUM_HAND = 21
MAX_HIT_HAND = 17
WINNING_SCORE = 5

def clear
  system('clear') || system('cls')
end

def prompt(message)
  puts ">> #{message}"
end

def input_continue
  prompt MESSAGES['continue']
  gets
end

def initialize_deck
  CARD_VALUES.product(CARD_SUITS).shuffle
end

def deal_card(player_dealer, deck)
  player_dealer[:hand] << deck.pop
end

def initial_hand(player_dealer)
  player_dealer[:hand].slice(0, 2)
end

def dealt_cards(player_dealer)
  player_dealer[:hand].slice(2, player_dealer[:hand].size - 2)
end

def display_cards(cards, show_first_card=true)
  card_display = ''
  cards.each_with_index do |card, index|
    if show_first_card || index > 0
      card_display << "#{card[0]} of #{card[1]}\n"
    else
      card_display << ''
    end
  end
  puts card_display
end

def calculate_total(player_dealer)
  total = 0
  count_aces = 0

  player_dealer[:hand].each do |card|
    if card[0] == 'Ace'
      total += 11
      count_aces += 1
    elsif card[0].to_i == 0
      total += 10
    else
      total += card[0].to_i
    end
  end
  count_aces.times { total -= 10 if total > MAXIMUM_HAND }
  player_dealer[:total] = total
end

def display_hand(player_dealer, show_first_card=true)
  calculate_total(player_dealer)
  puts "\n#{player_dealer[:name]}'s hand: "\
       "#{"Total = #{player_dealer[:total]}" if show_first_card}"
  display_cards(initial_hand(player_dealer), show_first_card)
  display_cards(dealt_cards(player_dealer)) unless dealt_cards(player_dealer).none?
end

def display_game(player, dealer, round_number, show_dealer_card=false)
  clear
  puts MESSAGES['open_message']
  puts "#{player[:name]}'s score: #{player[:score]}; "\
       "#{dealer[:name]}'s score: #{dealer[:score]}"
  puts "#" * 40
  puts "Round #{round_number}"
  display_hand(player)
  display_hand(dealer, show_dealer_card)
  puts ''
end

def busted?(player_dealer)
  calculate_total(player_dealer) > MAXIMUM_HAND
end

def winner(player, dealer)
  calculate_total(player)
  calculate_total(dealer)
  return dealer if busted?(player)
  return player if busted?(dealer)
  return dealer if dealer[:total] > player[:total]
  return player if player[:total] > dealer[:total]
end

def display_result(player, dealer)
  puts '-' * 40
  prompt MESSAGES['player_bust'] if busted?(player)
  prompt MESSAGES['dealer_bust'] if busted?(dealer)
  if winner(player, dealer) == player
    prompt MESSAGES['player_wins_round']
  elsif winner(player, dealer) == dealer
    prompt MESSAGES['dealer_wins_round']
  else
    prompt MESSAGES['tie_round']
  end
end

loop do
  round_number = 0
  player_stats = { name: 'Player', score: 0 }
  dealer_stats = { name: 'Dealer', score: 0 }
  deck = initialize_deck

  loop do
    round_number += 1
    player_stats[:hand] = []
    dealer_stats[:hand] = []

    2.times do
      deal_card(player_stats, deck)
      deal_card(dealer_stats, deck)
    end

    display_game(player_stats, dealer_stats, round_number)

    loop do
      hit_or_stay = ''
      prompt MESSAGES['hit_or_stay']
      loop do
        hit_or_stay = gets.chomp.downcase
        break if %w(h s).include?(hit_or_stay)
        prompt MESSAGES['invalid_choice']
      end

      break if hit_or_stay == 's'
      deal_card(player_stats, deck)
      display_game(player_stats, dealer_stats, round_number)

      break if busted?(player_stats)
    end

    unless busted?(player_stats)
      prompt MESSAGES['player_stay']
      input_continue
      display_game(player_stats, dealer_stats, round_number, true)

      while calculate_total(dealer_stats) < MAX_HIT_HAND
        prompt MESSAGES['dealer_hit']
        input_continue
        deal_card(dealer_stats, deck)
        display_game(player_stats, dealer_stats, round_number, true)
      end
    end

    display_result(player_stats, dealer_stats)
    if winner(player_stats, dealer_stats)
      winner(player_stats, dealer_stats)[:score] += 1
      break if winner(player_stats, dealer_stats)[:score] == WINNING_SCORE
    end
    input_continue
  end

  puts "-" * 40
  if winner(player_stats, dealer_stats)[:name] == 'Player'
    prompt MESSAGES['player_wins']
  elsif winner(player_stats, dealer_stats)[:name] == 'Dealer'
    prompt MESSAGES['dealer_wins']
  end
  puts '-' * 40

  play_again = ''
  prompt MESSAGES['play_again']
  loop do
    play_again = gets.chomp.downcase
    break if %w(y n).include?(play_again)
    prompt MESSAGES['invalid_choice']
  end
  break unless play_again == 'y'
end

puts MESSAGES['close']
