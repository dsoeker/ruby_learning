require 'pry'

GOES_FIRST = ['computer', 'player', 'choose']
INITIAL_MARKER = ' '.freeze
PLAYER_MARKER = 'X'.freeze
COMPUTER_MARKER = 'O'.freeze
WINNNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] + # Rows
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] + # Columns
                  [[1, 5, 9], [3, 5, 7]].freeze # Diagnols

# rubocop:disable Metrics/MethodLength, Metrics/AbcSize
def display_board(brd)
  system 'clear'
  puts "You're an #{PLAYER_MARKER}. Computer is #{COMPUTER_MARKER}."
  puts ""
  puts "     |     |"
  puts "  #{brd[1]}  |  #{brd[2]}  |  #{brd[3]}"
  puts "     |     |"
  puts "-----|-----|-----"
  puts "     |     |"
  puts "  #{brd[4]}  |  #{brd[5]}  |  #{brd[6]}"
  puts "     |     |"
  puts "-----|-----|-----"
  puts "     |     |"
  puts "  #{brd[7]}  |  #{brd[8]}  |  #{brd[9]}"
  puts "     |     |"
  puts ""
end
# rubocop:enable Metrics/MethodLength, Metrics/AbcSize

# This will govern who will start the game. set to 0 for computer to start, 1 player to start, or 2 for the player to choose who should start.
def who_starts
  GOES_FIRST[0]
end

def prompt(message)
  puts ">> #{message}"
end

def initialize_board
  new_board = {}
  (1..9).each { |num| new_board[num] = INITIAL_MARKER }
  new_board
end

def joinor(arr, delim=', ', word='or')
  arr[-1] = "#{word} #{arr.last}" if arr.size > 1
  arr.join(delim)
end

def empty_squares?(brd) # Grabs all the keys that are empty.
  brd.keys.select { |num| brd[num] == INITIAL_MARKER }
end

def player_places_piece!(brd)
  square = ''
  loop do
    prompt "Choose a square (#{joinor(empty_squares?(brd), ', ')}): "
    square = gets.chomp.to_i
    break if empty_squares?(brd).include?(square)
    prompt "Sorry, not a valid choice."
  end
  brd[square] = PLAYER_MARKER
end

def find_at_risk_square(line, board, marker)
  if board.values_at(*line).count(marker) == 2
    board.select { |key, value| line.include?(key) && value == INITIAL_MARKER }.keys.first
  else
    nil
  end
end

def computer_places_piece!(brd)
  square = nil
  WINNNING_LINES.each do |line|
    square = find_at_risk_square(line, brd, COMPUTER_MARKER)
    break if square
  end
  if !square
    WINNNING_LINES.each do |line|
      square = find_at_risk_square(line, brd, PLAYER_MARKER)
      break if square
    end
  end
  if !square
    square = empty_squares?(brd).sample
  end
  brd[square] = COMPUTER_MARKER
end
  
def board_full?(brd)
  empty_squares?(brd).empty?
end

def someone_won?(brd)
  !!detect_winner(brd)
end

# Using the '*' splat operator passes in the entire array without listing it out.
def detect_winner(brd)  
  WINNNING_LINES.each do |line|
    if brd.values_at(*line).count(PLAYER_MARKER) == 3
      return 'Player'
    elsif brd.values_at(*line).count(COMPUTER_MARKER) == 3
      return 'Computer'
    end
  end
  nil
end

def score_check?(player_score, computer_score)
  player_score == 5 || computer_score == 5
end

player_score = 0
computer_score = 0



loop do
  board = initialize_board # The board variable will be the basis for the entire program.

  loop do
    display_board(board)



    computer_places_piece!(board)
    break if someone_won?(board) || board_full?(board)
    
    display_board(board)

    player_places_piece!(board)
    break if someone_won?(board) || board_full?(board)
  end

  display_board(board)

  if someone_won?(board)
    prompt "#{detect_winner(board)} won!"
  else
    prompt "It's a tie!"
  end

  player_score += 1 if detect_winner(board) == 'Player'
  computer_score += 1 if detect_winner(board) == 'Computer'
  prompt 'No points awarded' if detect_winner(board) == nil 

  prompt "Score: Player = #{player_score}, Computer = #{computer_score}"
  break unless score_check?(player_score, computer_score) == false
  prompt "Play again (Y or N)"
  answer = gets.chomp
  break unless answer.downcase.start_with?('y')
end

prompt "Thanks for playing Tic Tac Toe."