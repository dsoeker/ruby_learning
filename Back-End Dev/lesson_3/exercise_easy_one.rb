
# Problem 1
#-------------
numbers = [1, 2, 2, 3]
numbers.uniq

puts numbers

# This should put the original array in the form of a string. The uniq method by itself will not mutate the caller, and puts will return a string.


# Problem 2
#--------------

# Describe the difference between ! and ? in Ruby. And explain what would happen in the following scenarios: 

# 1. what is != and where should you use it? 
  
  # This means does not equal. Maybe try using this when comparing a variable to a user input. 


# 2. put ! before something, like !user_name 

  # This again is equivalent to 'not'. So, if we put it in front of something like 'user_name', we'd likely be validating the variable.

# 3. put ! after something, like words.uniq! 

  # Putting '!' after a method makes it destructive. This, when used against something like an array, will alter the array permanently. 

# 4. put ? before something

  # This works as a ternary operator and is a type of conditional expression. 


# 5. put ? after something 

  # Usually it's part of the method name to indicate that it will return either true or false.

# 6. put !! before something, like !!user_name

  # Is used to turn into their boolean equivalent.


# Problem 3
#-------------
# Replace the word "important" with "urgent" in this string:

advice = "Few things in life are as important as house training your pet dinosaur."

advice.sub!('important', "urgent")

# or can use 'gsub!'

# Problem 4
#------------
# The Ruby Array class has several methods for removing items from the array. Two of them have very similar names. Let's see how they differ:

numbers = [1, 2, 3, 4, 5]

# What does the follow method calls do (assume we reset numbers to the original array between method calls)?

numbers.delete_at(1)
numbers.delete(1)

# The first deletes the value at index 1, while the other deletes the value 1.

# Problem 5
#-----------
# Programmatically determine if 42 lies between 10 and 100.

puts 'yes' if 42 > 10 && 42 < 100
(10..100).cover?(42)

# Problem 6
#--------------

famous_words = "seven years ago..."

# show two different ways to put the expected "Four score and " in front of it.

famous_words = "Four score and " + famous_words
famous_words.gsub(/^/, "Four score and ")
famous_words.prepend("Four score and ")
"Four score and " << famous_words

# Problem 7
#------------
# Fun with gsub:

def add_eight(number)
  number + 8
end

number = 2

how_deep = "number"
5.times { how_deep.gsub!("number", "add_eight(number)") }

p how_deep

# This gives us a string that looks like a "recursive" method call:

"add_eight(add_eight(add_eight(add_eight(add_eight(number)))))"
# If we take advantage of Ruby's Kernel#eval method to have it execute this string as if it were a "recursive" method call

eval(how_deep)

# what will be the result?
#42 

# Problem 8
#-------------
# If we build an array like this:

flintstones = ["Fred", "Wilma"]
flintstones << ["Barney", "Betty"]
flintstones << ["BamBam", "Pebbles"]
# We will end up with this "nested" array:

["Fred", "Wilma", ["Barney", "Betty"], ["BamBam", "Pebbles"]]

# Make this into an un-nested array.

flintstones.flatten!


# Problem 9
#------------
# Given the hash below

flintstones = { "Fred" => 0, "Wilma" => 1, "Barney" => 2, "Betty" => 3, "BamBam" => 4, "Pebbles" => 5 }

# Turn this into an array containing only two elements: Barney's name and Barney's number

flintstones.assoc("Barney")


# Problem 10
#--------------
# Given the array below

flintstones = ["Fred", "Barney", "Wilma", "Betty", "Pebbles", "BamBam"]

# Turn this array into a hash where the names are the keys and the values are the positions in the array.

flintstones_hash = {}
flintstones.each_with_index do |name, index|
  flintstones_hash[name] = index
end
