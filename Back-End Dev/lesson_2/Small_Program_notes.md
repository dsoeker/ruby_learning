# Lesson 2: Small Programs

## Ruby Style
---------------
1. Editors should be set to 2 spaces and indenting should be set to use spaces.

2. Anywhere that starts with a '#' is a comment.

3. Methods, Variables, or Files should all use 'snake_case' formatting for names.

4. Constant variables are always denoted in UPPERCASE letters.  

5. When working with 'do...end' blocks, opt for the '{}' when the code fits on one line.

6. When declaring class names, always use 'CamelCase' formatting.

- Consult the Ruby style guide for more info: https://github.com/bbatsov/ruby-style-guide

## Kickoff Lecture
-------------------
1. Learning to program procedurally
2. Object-Oriented Programming (OOP)
3. Other Stuff
  a. Testing
  b. blocks
  c. Tools
  d. Problem solving
  e. Developing Language Familiarity

4. Learning to Code versus Learning to Program
  - Ruby Grammar
  - Building Applications
  - Pseudo-code
    - Don't create large blocks of logic in Pseudo-code. Create small chunks, translate, then test.

5. Looking for Help
  - Search using the right phrases
    - Use the stack-trace
    - Reading the error messages
  - Stack Overflow
  - Ruby Docs

6. Asking for Help
  - Everyone is a coworker
  - Push everything to github
  - Forums
  - Chatrooms
  - TA office hours

## A Note on Style
--------------------
### The following will only be used in the code walkthroughs, and one does not need to apply to own code.

1. Parentheses
  - Parentheses are optional when invoking a method. For now, we are always going to use () when invoking a method. This will help differentiate between a method and variable call. So, for example: we will use ```get().chomp()``` instead of ```gets.chomp```.

2. puts and gets
  - When using the ```puts``` method, you look to display something. When using the ```gets``` method, you look to retrieve something from the user. These method calls will be displayed as: ```Kernal.puts()``` and ```Kernal.gets()``` to show that these are module methods.

## Walk-through: Calculator
-----------------------------
Goal - Build a calculator program that does the following:
  1. Asks for two numbers.
  2. Asks for the type of operation to perform: add, subtract, multiply or divide.
  3. Display the result.

**See the calculator.rb file for final product.**

## Pseudo-Code
------------------
When writing programming code, you're writing it for other programs to process. When writing Ruby, you're writing it for the Ruby interpreter to process. This is crucial during the run process. If Ruby code fails, the compiler complains, if the programming code fails, the entire program breaks down.

Pseudo-Code is meant for humans. It has no rigid formatting, and is not meant to be processed by machines.
Here's an example for a method that determines which number is greatest in a collection:

```
Given a collection of integers.

Iterate through the collection one by one.
  - save the first value as the starting value.
  - for each iteration, compare the saved value with the current value.
  - if the saved value is greater, or it's the same
    - move to the next value in the collection
  - otherwise, if the current value is greater
    - reassign the saved value as the current value

After iterating through the collection, return the saved value.
```

The point of this? **To load the problem into our brains**

Unfortunately, this takes some effort. And doing this while working with a programming Language is hard since your train of thought is constantly interrupted by syntax issues. Enter the two layers to solving any problem:

1. The logical problem domain layer.
2. The syntactical programming language layer.

Using Pseudo-Code helps us focus on the problem without worrying about the programming just yet. However, a problem arises. We can't be certain the Pseudo-Code is correct until we test the translation.

### Formal Pseudo-Code
-------------------------
We use some keywords to help breakdown the program logic into concrete commands.

| keyword         | meaning                             |
| --------------- | ----------------------------------- |
| START           | start of the program                |
| SET             | sets a variable we can use for later|
| GET             | retrieve the input from user        |
| PRINT           | displays output to user             |
| READ            | retrieve value from variable        |
| IF/ELSE IF/ELSE | show conditional branches of logic  |
| WHILE           | show looping logic                  |
| END             | end of the program                  |

We can use the above to act as a pseudo programming language, allowing us to be more relaxed about the syntax precision. Let's update the previous example with our formal syntax:

```
START

# Given a collection of integers called "numbers"

SET iterator = 1
SET saved_number = value within numbers collection at space 1

WHILE iterator < length of numbers
  SET current_number = value within numbers collection at space "iterator"
  IF saved_number >= current_number
    go to the next iteration
  ELSE
    saved_number = current_number

  iterator = iterator + 1

PRINT saved_number

END
```

### Translating Pseudo-Code to Program Code
----------------------------------------------
Now let's take our example and translate it to Ruby:

```Ruby
def find_greatest(numbers)
  saved_number = nil

  numbers.each do |num|
    saved_number ||= num  # assign to first value
    if saved_number >= num
      next
    else
      saved_number = num
    end
  end

  saved_number
end
```

While this solution translates perfectly into Ruby, there will be times when it doesn't and we need to rewrite the code, sometimes more than once. Remember, this isn't the solution, but a way to test the logic in a format that's easier for the brain to interpret.

### Practice Pseudo-Code
-------------------------
1. A method that returns the sum of two integers.

```
START

GET number 1 and number 2

# Given two numbers from the user

SET result = number 1 + number 2
READ result of addition operation

PRINT result of addition operation

END
```

2. A method that takes an array of strings, and returns a string that is all those strings concatenated together

```
START

SET an array with some string values in it

# Given the array with values you want to concatenate
READ array with values and use .join() method to concatenate

PRINT concatenated_result

END
```

3. A method that takes an array of integers, and returns a new array with every other element

```
START

SET array with integer values.

# Given the array with integers
SET index_iterator = 0
SET output array to an empty array
WHILE index_iterator <= length of array with integers
  IF value of index_iterator does have a remainder when divided by 2
    READ value and push it to the new array
index_iterator += 1

PRINT the new array

END
```

## Flowchart
---------------
When creating Flowcharts, we use a few components to signify the different steps in the program.

1. Ellipse = Start / Stop
2. Rectangle = Processing Step
3. Parallelogram = Input / Output
4. Diamond = Decision
5. Circle = Connector

There are also arrows that connect one component to another, these represent the "flow" of the logic. This helps us map the step-by-step logic our program would need to solve the problem. This is known as a *imperative or procedural* way of solving something. In high-level programming, things like iterating over a collection are encapsulated into a method. Take the ```each``` method, this is a declarative way to solve a problem.

With this process you'll be forced to think like a computer. This will achieve a higher level declarative syntax.

### A Larger Problem
-----------------------
Let's suppose we ask the user to give us N collections of numbers. We want to take the largest number out of the each collection, and display it.

Pseudo-Code:

```
while user wants to keep going
  - ask the user for a collection of numbers
  - extract the largest one from that collection and save it
  - ask the user if they want to input another collection

return saved list of numbers
```

The line that says ```extract``` causes some concern since there is a lot to go along with this. We would want to outline the subprocess to go along with this:

```
while user wants to keep going
  - ask the user for a collection of numbers
  - iterate through the collection one by one.
    - save the first value as the starting value.
    - for each iteration, compare the saved value with the current value.
    - if the saved value is greater, or it's the same
      - move to the next value in the collection
    - otherwise, if the current value is greater
      - reassign the saved value as the current value

  - after iterating through the collection, save the largest value into the list.
  - ask the user if they want to input another collection

return saved list of numbers
```
However, ^ this is too long. Once Pseudo-Code gets lengthy, we start loose confidence in the efficiency. Therefor, it's better to extract a logical grouping into subprocesses and tackle the various pieces separately.

Here's the final product in formal Pseudo-Code:

```
START

SET large_numbers = []
SET keep_going = true

WHILE keep_going == true
  GET "enter a collection"
  SET collection
  SET largest_number = SUBPROCESS "extract the largest one from that collection"
  large_numbers.push(largest_number)
  GET "enter another collection?"
  IF "yes"
    keep_going = true
  ELSE
    keep_going = false
  IF keep_going == false
    exit the loop

PRINT large_numbers

END
```
Diving further into Pseudo-Code and Flowcharts to help you dissect logic of a problem, you'll be trying to figure out how detailed the chart and words should be, and what can be extracted to sub-process.

Quick list of steps

1. Start at a high level, using declarative syntax.
2. With the high level done, it's time to move to the imperative syntax and outline specifics.


# Rubocop
-----------
A Static code analyzer that offers advice on style format and logical errors. Has a modular framework using a pluggable architecture to insert additional rules to enforce. In Rubocop parlance, these rules are called *cops*. Cops are further grouped into *departments*.

## Style format
------------------
Coding style is mostly a matter of opinion, but in general, Rubyists have a standard set of conventions (Check the style guide).

For example, if your code isn't indented with 2 spaces, the ```IndentationWidth``` cop will complain.

## Code Linting
-----------------
A code linter is a program that inspects your code for possible logical errors or code smells. This is a defense against obvious problems. Rubocop has some basic code linting functionality built-in.

For example, if your code instantiates an unused local variable, the ```UselessAssignment``` cop will complain.

### Code outputs
-----------------
1. The first line will tell you the number of files that were inspected.
2. The second line has a few options.
  - C = Convention, a convention was broken
  - W = Warning
  - E = Error
  - F = Fatal Error
3. The list of offenses comes next with the code where the offense took place listed for you.

# Walk-Through: Refactoring Calculator
----------------------------------------
Things we might want to change

1. Add the '=>' in front of every puts statement.
```ruby
def prompt(message)
  puts "=> #{message}"
end
```

2. Refactor the If statement to a case statement
```ruby
result = case operator
  when '1'
    number1.to_i + number2.to_i
  when '2'
  when '3'
  when '4'
```

3. Looping construct that validates the entry as a number:
```ruby
def valid_number?(number)
  num.to_i != 0
end
number1 = '' # Must initialize number outside the loop
loop do
  prompt()
  number = gets.chomp

  if valid_number?(number1)
    break
  else
    prompt('something wrong.')
  end
end
```

4. Create a main loop that envelopes the entire code. This needs another prompt() at the end asking if another operation is to be performed.
```ruby
break unless answer.downcase().start_with?('y')
```

5. Have the user enter his/her name.
```ruby
prompt("Welcome to the calculator! Please enter your name: ")
name = ''
loop do
  name = gets.chomp

  if name.empty?()
    prompt("Make sure to use a valid name.")
  else
    break
  end
end
```
Say hi to the user, but leave outside of the main loop to avoid asking for the name every time.

6. Add an array of strings to ensure the user actually inputs a number
```ruby
if %w(1 2 3 4).include?(operator)
  break
else
  prompt('Must choose: 1, 2, 3, or 4')
end
```
make sure the operator variable is declared outside the loop.

6. Change the prompt with the number choices in a message format.
```ruby
operator_prompt = <<-MSG
  What operation would you like to perform?
  1) add
  2) subtract
  3) multiply
  4) divide
MSG
prompt(operator_prompt)

7. Lastly, run rubocop against this file and check for offenses.

# Debugging
-------------
The core of prgramming. 

### Temperament
-----------------
If the key to programming is debugging, then the key to debugging is being patient and logical. 

If you're walking to a bus stop and the bus leaves right before you get there, how do you react? Someone who has the programming temperament should figure out:

1. When the next bus comes.
2. If there's an alternative path of transfers you can take.
3. Other alternative forms of transportation.

Dealing with the feelings of frustration is the first critical aspect of learning to program.

### Reading Error Messages
-----------------------------
When you run into an error, you'll likely hit a wall of text known as a **Stack Trace** and is a critical piece to the puzzle.

The trick in dealing with these is to know where to look. The more you train yourself to only look at the relevant parts of the error, the faster you'll be at finding it.

### Online Resources
----------------------
1. Search Engine
2. Stack Overflow
3. Ruby Documentation

### Steps to Debugging
------------------------
The debugging process varies tremendously from person to person, but here are some steps to follow:

1. Reproduce the error
2. Determine the boundaries of the Error
3. Trace the code
4. Understand the Problem Well
5. Implement a Fix
6. Test the Fix

### Techniques for Debugging
-------------------------------
1. Line by Line
2. Rubber Duck
3. Walking Away
4. Using Pry
5. Using a debugger

### Summary
------------
debugging is the most important skill you will learn. Focus on development of a patient, systematic temperament; carefully read error messages; use all the wonderful resources at your disposal; approach debugging in sequential steps; and use Pry.


# Assignment: Mortgage / Car Loan Calculator
-----------------------------------------------
Take everything you've learned so far and build a mortgage calculator (or car payment calculator -- it's the same thing).

You'll need three pieces of information:

  - the loan amount
  - the Annual Percentage Rate (APR)
  - the loan duration
  From the above, you'll need to calculate the following two things:

  - monthly interest rate
  - loan duration in months
  - You can then use the formula described here: http://www.mtgprofessor.com/formulas.htm

Finally, don't forget to run your code through Rubocop.

Hints:

Figure out what format your inputs need to be in. For example, should the interest rate be expressed as 5 or .05, if you mean 5% interest?
If you're working with Annual Percentage Rate (APR), you'll need to convert that to a monthly interest rate.
Be careful about the loan duration -- are you working with months or years? Choose variable names carefully to assist in remembering


# Coding Tips
---------------
Specific items seen in code that you can fix.

## Dramatic Experience and Retaining Knowledge
--------------------------------------------------
- The only way to retain information is to pay with time.
- Time spent debugging is never time wasted.

## Naming Things
------------------
- Variables are not named for how they are set, but for what they actually store. 
- When naming variables, try to capture the intent of the variable.

## Naming Conventions
----------------------
- In Ruby, use 'snake_case' when naming everything, except classes which are 'CamelCase' or constants, which use 'UPPERCASE'.

## Mutating Constants
------------------------
- CONSTANTS should be immutable

## Methods
------------
- Ensure that methods do one thing and their responsiblities are limited.
- Methods should be 10 - 15 lines, max.
- Methods should not display something to output as well as return a value.
- A method should return something with no side effects or only perform side effects with no return.
  - In the standard Ruby library, many methods end with a '!' to signify side effects.

## Methods should be at the same level of abstraction
-------------------------------------------------------
- When working with a method, you should be able to mentally extract the method from the larger program, and just work with the method in isolation.
  - Working this way will comparmentalize your focus.

## Method names should reflect mutation
------------------------------------------
- The less you have to remember and the less other people have to remember while looking at your code, the better.
- Use naming conventions, even in your own code, to signify which types of methods mutate vs which return values. 
- The more you have to think about the method, the harder it is to use it.
- If you have a method that is convoluted, the program's logic may be more complex, or you may not fully understand the problem.
- Approach the problem similar to writing:
  - This is a very similar process to writing. Your first draft is almost exploratory, dumping out ideas all over the place. As your narrative comes into more focus, the structure of your piece can become more organized and clean.

## Displaying Output
-----------------------
- Methods are like black-boxes. It takes some stuff (input) and returns some value (output) to you. They should be very contained and you should know what they do without having to looking up the implementation. Coding will be much easier if you follow these general guidelines.

## Miscellaneous Tips
-----------------------
- Don't prematurely exit the program. You program should probably.
- Use 2 spaces, not tabs when indenting. 
- Name your methods from the perspective of using them later. That is, think about how you would like to invoke them. Think about the implementation later. 

- While loops vs Do/While

While example:
```ruby
answer = ''
while answer.downcase != 'n' do
  puts "Continue? (y/n)"
  answer = gets.chomp
end
```

That certainly would work, but a slightly better implementation could be to use a "do/while" loop:

```ruby
loop do
  puts "Continue? (y/n)"
  answer = gets.chomp
  break if answer.downcase == 'n'
end
```

Here, the entire code is contained in the loop, and it's slightly easier to reason with. You could even do without the ```answer``` variable and use the user's input (i.e. ```gets.chomp```) in the if condition directly, but using ```answer``` is fine -- remember, clarity over terseness.

## Truthiness
---------------
Ruby is very liberal about what 'true' means, thus: 
```ruby
if user_input == true
# could be just
if user_input
```
However, if you want to just gaurd against ```nil```, you could write: ```user_input != nil``` since nil evaluates to false.

## Approach to learning
--------------------------
Don't be demoralized if you do it once and you can't remember most of it. That's normal. Keep moving forward, and don't be afraid to spend time gaining valuable experience.


# Variable Scope
-----------------
Local Variables - are the most common variables you will come across and obey all scope boundaries. These variables are declared by starting the variable name with neither ```$``` nor ```@```, as well as not capitalizing the entire variable name.

The two major areas where we encounter local variable scoping rules are related to ***blocks*** and ***methods**.

## Variables and Blocks
------------------------
```ruby
[1, 2, 3].each do |num|
  puts num
end
```
The part we call the ***block*** is this:

```ruby
do |num|
  puts num
end
```
However, the ```do...end``` can be replaced by ```{...}```

```ruby
{ |num|
  puts num
}
```
Many Rubyists do prefer to use ```do...end``` for multi-line blocks and ```{}```

- You can think of a scope created by a bloack as an ***inner scope***. Nested blocks will create nested scopes. A variable's scope is determined by where it is initialized.

```ruby
loop do       # the block creates an inner scope
  b = 1
  break
end

puts b 
# => NameError: undefined local variable or method `b' for 
main:Object
```
^ Since the variable 'b' was declared inside the method, the call to 'b' later in the program will return an error.

```ruby
2.times do
   a = 'hi'
   puts a      # 'hi' <= this will be printed out twice due to the loop
 end

 loop do
   puts a      # => NameError: undefined local variable or method `a' for main:Object
   break
 end

 puts a
 ```
^ The last two ```puts a``` throw an error because the initial ```a = 'hi'``` is scoped within the first block of code. Peer blocks cannot reference variables initialized in other blocks. This means that we could re-use the variable name ```a``` in the second block of code, if we wanted to.


### Nested Blocks
```ruby
a = 1           # first level variable

loop do         # second level
  b = 2

  loop do       # third level
    c = 3
    puts a      # => 1
    puts b      # => 2
    puts c      # => 3
    break
  end

  puts a        # => 1
  puts b        # => 2
  puts c        # => NameError
  break
end

puts a          # => 1
puts b          # => NameError
puts c          # => NameError
```
^ Trying to reference variables that appear in the inner blocks from the outer ones will return an error. 

## Variable Shadowing
----------------------
```ruby
[1, 2, 3].each do |n|
  puts n
end
```
The block is the ```do...end```, and the block parameter is captured between the ```|``` symbols. In the above example, the block parameter is ```n```, which represents each element as the each method iterates through the array.

But what if we had a variable named ```n``` in the outer scope? We know that the inner scope has access to the outer scope, so we'd essentially have two local variables in the inner scope with the same name. When that happens, it's called ***variable shadowing***, and it prevents access to the outer scope local variable. See this example:

```ruby
n = 10

1.times do |n|
  n = 11
end

puts n          # => 10
```
Choosing long and descriptive variable names is one simple way to ensure that you don't run into any of these weird scoping issues. 

### Variables and Methods
---------------------------
- If block scopes "leak", then method scopes are entirely self contained. The only variables methods have access to must be passed in to the methods. 
- Methods have no notion of "outer" or "inner" scope -- you must explicitly pass in any parameters to methods.
- You must pass in local variables to methods.

### Blocks within Methods
---------------------------
- Unsurprisingly, the rules of block scope remain in full effect even if we're working inside a method.

```ruby
def some_method
  a = 1
  5.times do
    puts a
    b = 2
  end

  puts a
  puts b
end

some_method
```

### Constants
---------------
Surprisingly, the scoping rules for constants is not the same as local variables. In procedural style programming, constants behave like globals. Here's an example:

```ruby
USERNAME = 'Batman'

def authenticate
  puts "Logging in #{USERNAME}"
end

authenticate    # => Logging in Batman
```

Constants are said to have lexical scope, which will have more meaningful consequences when we get to object oriented programming. For now, just remember that constants have different scoping rules from local variables.

### Summary
-------------
Understanding variable scope is one of the most challenging and important aspects to learning to program. Make sure you know how local variable scope works with regards to blocks and methods. Play around with various scenarios until you feel comfortable. It's likely you'll forget these rules, but the most important thing is to be able to quickly jump in irb or open up your editor and refresh your memory.

Remember that different variable types will have different scoping rules, and the only other variable type you should run into right now besides local variables are constants.

# Pass by Reference vs Pass by Value
---------------------------------------
The discussion stems from trying to determine what happens to objects when passed into methods. In most programming languages, there are two ways of dealing with objects passed into methods. You can either treat these arguments as "references" to the original object or as "values", which are copies of the original.

## What does pass by 'value' mean?
--------------------------------------
In C, when you 'pass by value', the method only has a ***copy*** of the original object. Operations performed on the object within the method have no effect on the original object outside of the method.

```ruby
def change_name(name)
  name = 'bob'      # does this reassignment change the object outside the method?
end

name = 'jim'
change_name(name)
puts name           # => jim
```

It looks like it's by value, since re-assigning the variable only affected the method-level variable, and not the main scope variable.

## What does pass by 'reference' mean?
-----------------------------------------
If Ruby was pure "pass by value", that means there should be no way for operations within a method to cause changes to the original object. But you can plainly do this in Ruby. For example:

```ruby
def cap(str)
  str.capitalize!   # does this affect the object outside the method?
end

name = "jim"
cap(name)
puts name           # => Jim
```

This implies that Ruby is "pass by reference", because operations within the method affected the original object.

```ruby
def cap(str)
  str.capitalize
end

name = "jim"
cap(name)
puts name           # => jim
```

## Whay Ruby does
-------------------
Ruby exhibits a combination of behaviors from both "pass by reference" as well as "pass by value". Some people call this pass by reference of the value or call by sharing. Whatever you call it, the most important concept you should remember is:

when an operation within the method mutates the caller, it will affect the original object

It's a guarantee that you will run into this issue over and over in your programming career. Learn to train your brain to recognize this as a potential problem, and you'll be able to debug future problems much faster.

## Variables and pointers
---------------------------
variables are pointers to physical space in memory. In other words, variables are essentially labels we create to refer to some physical memory address in your computer. 


# Walk-through: Rock, Paper, Scissors
---------------------------------------
See rock_paper_scissors.rb

# Coding Tips 2
-----------------
- New lines are important visual cues in the program.
- Making your code readable is of paramount importance, not only for others, but for future self.

```ruby
# bad

name = ''
puts "Enter your name: "
loop do
  name = gets.chomp
  break unless name.empty?
  puts "That's an invalid name. Try again:"
end
puts "Welcome #{name}!"
puts "What would you like to do?"
```
Use some lines to separate the different concerns in the code.

```ruby
# better

name = ''

puts "Enter your name: "
loop do
  name = gets.chomp
  break unless name.empty?
  puts "That's an invalid name. Try again:"
end

puts "Welcome #{name}!"
puts "What would you like to do?"
```
Visually, you can quickly see where the ```name``` variable is initialized. This code has also been broken down into 3 parts:

- Variable initialization
- User input and validation
- Using the variable

This also simplifies debugging.

### Should a method return or display?
----------------------------------------
Key concept here is, understanding is a method returns a value, or has side effects, or both.

"side effects" means wither displaying something to the output or mutating the object. The below example of methods have side effects:

```ruby
# side effect: displays something to the output
# returns: nil

def total(num1, num2)
  puts num1 + num2
end

# side effect: mutates the passed-in array
# returns: updated array

def append(target_array, value_to_append)
  target_array << value_to_append
end
```
And something with no side effects:

```ruby
# side effect: none
# returns: new integer

def total(num1, num2)
  num1 + num2
end
```
In general, if a method has both side effects and a meaningfull return value, it's a red flag. Try to avoid writing methods that do this, as it will be very difficult to use these methods in the future.

### Name methods appropriately
-------------------------------
One way to help yourself remember what each method does is to choose good method names. If you have some that output values, then preface those methods with ```display_``` or ```_print```. Something like: ```print_total``` should be self explanatory. 

- If you find yourself constantly looking at a method's implementation every time you use it, it's a sign that the method needs to be improved.
  - This comes back to the idea: a method should do one thing, and be name appropriately.

- Don't write a method that mutates, outputs and returns a meaningful value. Make sure your methods only do one of those things.

### Don't mutate the caller iteration
---------------------------------------
Suppose we have an array of strings and we want to iterate over that array and print out each element. We could do something like this:

```ruby
words = %w(scooby doo on channel two)
words.each {|str| puts str}
```

Now suppose we do something weird in the each code block, and we want to remove the element as we're iterating.

```ruby
words = %w(scooby doo on channel two)
words.each {|str| words.delete(str)}
puts words.inspect        # => ["doo", "channel"]
```
That is very strange -- shouldn't every element be deleted? We should be expecting an empty array, or even perhaps nil. The lesson here is:

```Don't mutate a collection while iterating through it.```

You can, however, mutate the individual elements within that collection, just not the collection itself. Otherwise, you'll get unexpected behavior.

### Variable Shadowing
-------------------------
This occurs when you choose a local variable in an inner scope that shares the same name as an outer scope. It's incredibly easy to make this mistake, and essentially prevents you from accessing the outer scope variable from an inner scope.

Suppose we have an array of names, and we want to append a last name to them. We could write some code like this:

```ruby
name = 'johnson'

['kim', 'joe', 'sam'].each do |name|
  # uh-oh, we cannot access the outer scoped "name"!
  puts "#{name} #{name}"
end
```
- A local variable cannot have the same name as a variable in a method, simple as that.

Rubocop will catch these errors.

### Don't use assignment in a conditional
-------------------------------------------
- Don't assign variables in a conditional. This is confusing and others may think this is a bug. As a convention, if you must do this, wrap the assignment in parentheses. This will signify to future programmers that you know what you're doing and this is done on purpose.

```ruby
# bad

if some_variable = get_a_value_from_somewhere
  puts some_variable
end

# good

some_variable = get_a_value_from_somewhere
if some_variable
  puts some_variable
end
```

Regardless, this is still not recommended.


### Use underscore for unused parameters
-------------------------------------------
Suppose you have an array of names, and you want to print out a string for every name in the array, but you don't care about the actual names. In those situations, use an underscore to signify that we don't care about this particular parameter.

```ruby
names = ['kim', 'joe', 'sam']
names.each { |_| puts "got a name!" }
```

Or, if you have an unused parameter when there are multiple parameters:

```ruby
names.each_with_index do|_, idx|
  puts "#{idx+1}. got a name!"
end
```

### Gain experience through struggling
-----------------------------------------
We can't say this enough: spend the time programming. Learn to debug through problems, struggle with it, search for the right terms, play around with the code, and you'll be able to transform into a professional developer. Because that's exactly what professional developers do on a daily basis.


# Expand the RPS Game
-----------------------
Lizard Spock

This is a variation on the normal Rock Paper Scissors game by adding two more options - Lizard and Spock. The full explanation and rules are here. There's also a hilarious Big Bang Theory video about it here.

- The goal of this bonus is to add Lizard and Spock into your code.

- Typing out the full word "rock" or "lizard" is quite tiring. Modify the program so that the user just has to type "r" for "rock". Note that if you did bonus #1, you'll have two words that starts with "s". How do you resolve that?

Use 'sc' for scissors and 'sp' for spock

- Keep score of how many times the player or computer has won. Make it so that whoever reaches 5 points first wins. Note: you might be tempted to just add some incrementing logic to the display_results method. Don't do this. Remember to keep your methods simple and that they should only do one thing.








