require 'yaml'
MESSAGES = YAML.load_file('mortgage_messages.yml')

def prompt(message)
  puts ">> #{message}"
end

def amount_check?(number_entered)
  number_entered.to_f.to_s == number_entered
end

def year_check?(duration)
  duration.to_i.to_s == duration
end

def monthly_payment(loan_amount, monthly_interest, number_of_months)
  monthly_numer = monthly_interest * (1.0 + monthly_interest)**number_of_months
  monthly_denom = (1.0 + monthly_interest)**number_of_months - 1.0
  final_value = loan_amount.to_f * (monthly_numer / monthly_denom)
  final_value.to_f.round(2)
end

prompt(MESSAGES['welcome'])

loop do
  loan_amount = ''
  loop do
    prompt(MESSAGES['enter_loan'])
    loan_amount = gets.chomp
    break if amount_check?(loan_amount)
  end

  apr_amount = ''
  loop do
    prompt(MESSAGES['enter_rate'])
    apr_amount = gets.chomp
    break if amount_check?(apr_amount)
  end

  number_of_years = ''
  loop do
    prompt(MESSAGES['enter_years'])
    number_of_years = gets.chomp
    break if year_check?(number_of_years)
  end

  monthly_int = (apr_amount.to_f / 100.0) / 12.0
  num_months = number_of_years.to_f * 12.0
  final_monthly_payment = monthly_payment(loan_amount, monthly_int, num_months)

  prompt(MESSAGES['breakdown_open'])
  puts '--' * 20
  puts "Loan Amount: $#{loan_amount}"
  puts "Annual Percentage Rate: #{apr_amount}%"
  puts "Number of Years: #{number_of_years}"
  puts '--' * 20
  prompt("Your payment would be: $#{final_monthly_payment} / month")
  puts ''

  prompt(MESSAGES['run_again'])
  run_again = gets.chomp
  break unless run_again.downcase().start_with?('y')
end

prompt(MESSAGES['goodbye'])
