require 'yaml'
MESSAGES = YAML.load_file('rps_bonus_messages.yml')
VALID_CHOICES = { 'r' => 'Rock', 'p' => 'Paper','s' => 'Scissors', 'l' => 'Lizard', 'sp' => 'Spock' }.freeze
WIN_COMBINATIONS = [%w(s p), %w(p r), %w(r l), %w(l sp), %w(sp s), %w(s l), %w(l p), %w(p sp), %w(sp r), %w(r s)].freeze
WINNING_TOTAL = 5

def prompt(message)
  puts "=> #{message}"
end

def clear_screen
  system('clear') || system('cls')
end

def separator(length)
  puts '--' * length.to_i
end

def win_check?(choice_one, choice_two)
  WIN_COMBINATIONS.include?([choice_one, choice_two])
end

def display_results(player_decision, computer_decision)
  if win_check?(player_decision, computer_decision)
    prompt(MESSAGES['award_player'])
  elsif win_check?(computer_decision, player_decision)
    prompt(MESSAGES['award_computer'])
  else
    prompt(MESSAGES['award_tie'])
  end
end

computer_score = 0
player_score = 0

clear_screen
separator(25)
puts MESSAGES['welcome']
separator(22)
puts MESSAGES['instructions']
puts MESSAGES['rules']
separator(22)

round_counter = 1
loop do
  player_choice = ''
  loop do
    prompt("Round: #{round_counter}. Make your choice: ")
    puts MESSAGES['choice_list']
    player_choice = gets.chomp.downcase
    break if VALID_CHOICES.key?(player_choice)
  end

  computer_choice = VALID_CHOICES[VALID_CHOICES.keys.sample]
  computer_choice_key = VALID_CHOICES.key(computer_choice)
  prompt("Player chooses: #{VALID_CHOICES[player_choice]}")
  prompt("Computer chooses: #{computer_choice}")
  display_results(player_choice, computer_choice_key)
  separator(22)

  player_score += 1 if win_check?(player_choice, computer_choice_key)
  computer_score += 1 if win_check?(computer_choice_key, player_choice)
  break if player_score == WINNING_TOTAL || computer_score == WINNING_TOTAL

  prompt("Current Score: Computer: #{computer_score}, Player: #{player_score}")
  round_counter += 1
end

separator(25)
prompt("Final Score: Computer: #{computer_score}, Player: #{player_score}")
prompt('Goodbye')
