require 'yaml'

=begin
Goal - Build a calculator program that does the following:
  1. Asks for two numbers.
  2. Asks for the type of operation to perform: add, subtract, multiply or divide.
  3. Display the result.
=end
MESSAGES = YAML.load_file('calculator_messages.yml')


def prompt(message)
  puts "=> #{message}"
end


def integer?(number)
  number.to_i.to_s == number
end

def float?(number)
  number.to_f.to_s == number
end

def valid_number?(number)
  integer?(number) || float?(number)
end

def operation_to_messge(operation)
  number = case operation
  when '1'
    "adding"
  when '2'
    "subtracting"
  when '3'
    "multiplying"
  when '4'
    "dividing"
  end
  return number
end

prompt(MESSAGES['welcome'])
sleep(2)
name = ''

loop do 
  name = gets.chomp

  if name.empty?
    prompt(MESSAGES['invalid_name'])
  else
    break
  end
end

prompt("Hello, #{name}.")
number1 = ''
number2 = ''
loop do 
  loop do 
    prompt(MESSAGES['first_number'])
    number1 = gets.chomp

    if valid_number?(number1)
      break
    else
      prompt(MESSAGES['invalid_number'])
    end
  end 

  loop do 
    prompt(MESSAGES['second_number'])
    number2 = gets.chomp

    if valid_number?(number2)
      break
    else
      prompt(MESSAGES['invalid_number'])
    end
  end

  operator_prompt = <<-MSG
    What operation would you like to perform?
    1) Add
    2) Subtract
    3) Multiply
    4) Divide
  MSG
  prompt(operator_prompt)

  operation_num = ''
  loop do 
    operation_num = gets.chomp

    if %(1 2 3 4).include?(operation_num)
      break
    else
      prompt(MESSAGES['invalid_operator'])
    end
  end

  result = case operation_num
            when '1'
              number1.to_i + number2.to_i
            when '2'
              number1.to_i - number2.to_i
            when '3'
              number1.to_i * number2.to_i
            when '4'
              number1.to_f / number2.to_f
  end

  prompt("Now #{operation_to_messge(operation_num)} your numbers...")
  sleep(1)
  prompt("Your result is: #{result}")
  sleep(1)
  prompt(MESSAGES['run_again'])
  run_again = gets.chomp

  break unless run_again.downcase().start_with?('y')
end

prompt(MESSAGES['goodbye'])
