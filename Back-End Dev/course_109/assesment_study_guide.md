# Videos
--------------

## Part 1
1. Syntactical Sugar
-----------------------------------
puts "hello"
	- Parentheses are optional

str < Just typing this, sometimes it's hard to tell what this is. Could be...

A method
def str
	'method stuff'
end

Or a variable
str = ""

----------

def str
	"a method"
end
p str < Printing out the return value of the method invocation

str = 'a string'
p str < Printing out the value of the str variable

### If you have a method and a local variable of the same name in the program, Ruby will call the variable if the bare word is called. The method will be called if the () are added. p str vs p str(). 

### A variable call out side a method doesn't necessarily make it global. 

2. Where does the code come from
-----------------------------------
puts "hello"
	- Allows you to output something to StdOut.

Core API - What is loaded into the Ruby Runtime, organized into classes on the Ruby-doc site
	- Kernel: One of the most important classes in Ruby
		Kernel.puts
	- All methods in the Core API can be invoked without anything extra.

Standard API - Comes with Ruby, but not automatically loaded.
	- Must use the require at the top of the program.

External Libraries - things like gems.


3. (Local) Variable Scope
----------------------------
arr = [1,2,3,4]

counter = 0
sum = 0
loop do
	sum += arr[counter]
	counter += 1
	break if counter == arr.size
end


### Variables initialized outside a block can be accessed by inside the block.
### Variables initialized inside the block are not accessible outside the block.

str = "hello"

loop do
	str = "world"
	break
end

puts str 

### The above will output "world" since the variable that was initialized outside the block was reassigned inside the block. If str = "hello" is removed, the program outputs an error since the variable can't be found.


---------------------------

## Part 2

1. Pass by Reference vs Pass by Value
------------------------------------------

a = "hello"
b = a # a and b point to the same object

b << " world"

# -- 2 variables, 1 object

puts a
puts b

a += b

# -- 2 variables, 2 objects

# a = "hey" would be a reassignment
b << " universe"

puts a
puts b

The simple idea of a destructive method is not so if there is a reassignment to the variable comes before the destructive method.

---------------------------

## Part 3

1. Arrays and Hashes
------------------------------------------
Collection - A type of data sructure that could be an array, hash, or string

What's a Hashes?
A series of elements that are retrievable by using key-value pairs
ex {:a => 1} or for symbols {a: 1} (symbols are immutable strings)
What's the difference between array and hash?
	- Order
	- index vs key retrieval
	- arrays can have duplicates, hash keys are unique
	
Blocks are chunks of code we can pass into a method (each).

Arrays

arr = [1,2,3,4,5,6,7,8,9,10]

arr.each do |n|
	puts n
end

vs,

idx = 0
loop do
	puts arr[idx]
	idx += 1
	break if idx == arr.size
end

arr.select {|n| n.odd? } # Select returns a new array based on the block's *return value*. If the return value evaluates to true, then the element is selected.)

odds = arr.select do |n|
	n + 1
	puts n
end

-----------------
1 Local variable scope, especially how local variables interact with blocks and methods
	- If a variable is assigned within a method, then the scope of that variable is confined to that method.
		 
		def foo(x)
  		if(x > 5)
    		bar = 100
  		end
			puts bar
		end
		
		^ In the above, if x is less than 5, then bar would equal '' since there's declaration of bar outside of the if statement inside the method.
			If bar was greater than 5, then we would see bar be set to the value declared inside the if statement.

	- Within a block, the logic is much the same. However, if the variable is initialized outside of the block, then we can call it outside the block as well.

		new_var = '' 
		loop do
			new_var += new_value
			break
		end
		puts new_var
	
		^ Since we initialized the 'new_var' variable outside the loop, we can call it after the loop completes. Otherwise, the 'new_var' would return an error as an unknown local variable.
		
		- Inner scope can access variables initialized in an outer scope, but not vice versa.

2. How passing an object into a method can or cannot permanently change the object
	
	- It all depends on the operator that is applied to variable within the method or block. Some operators have the power to mutate the caller, while others will only alter the variable that is within the scope of the method. 
	
	- For strings, something like '+=' is not mutating, but '<<' is. Also, for methods, anything with a '!' operator will permenantely change a variable's assigned value.

3. Working with collections (Array, Hash, String), and popular collection methods (each, map, select, etc). Study these methods carefully.

	- Arrays: Ordered lists or values that can be of any type.
		- include?: Checks to see if the argument given is included in the array. It has a question mark at the end of it which usually means that it will return a boolean value.
		
		- flatten: Can be used to take an array that contains nested arrays and create a one-dimensional array.
		
		- each_index: Iterates through the array much like the each method, however the variable represents the index number as opposed to the value at each index.
		
		- each_with_index: Gives us the ability to manipulate both the value and the index by passing in two parameters to the block of code
		
		- sort: Returns a sorted array and can be destructive.
		
		- product: Used to combine two arrays in an interesting way. It returns an array that is a combination of all elements from all arrays
		
		- each: The each method works on objects that allow for iteration and is commonly used along with a block. If given a block, each runs the code in the block once for each element in the collection and returns the collection it was invoked on. If no block is given, it returns an Enumerator.
		
		- map: Works in much the same way as each, but will create a new array with the returned values.
		
		- select: Will return true or false for each item in the collection depending on the condition set inside the block.
			
			list = (1..10).to_a
			p list.select { |i| i >= 3 && i <= 7 }

	- Hashes: Key-value pairs that are retrievable and have no explicit order.

4. Variables as pointers
	
	- Variables don't actually hold any value. They only point to an assignment. 

5. Puts vs return
	
	- When Ruby outputs something to the screen, it doesn't necessarily return the same value. For instance, using 'puts' will output the item you tell it to, but will also return 'nil'. Expressions do something, but also return something.

6. False vs nil
 
	- nil: Is a way to express nothing, an empty variable, or something without a specific type.
	- False: Is a boolean operator. 
 
	Pretty much everything in Ruby will evaluate to true except for false or nil.

7. Implicit return value of methods and blocks

	- Any line that is the last in a method or block that is not preceeded by a return statement will be the implied return value. However, if you include the return statement, then the method or block will return the value wherever this keyword is met.


Assessment take aways

1. Destructive changes permanently alter the object which the variable points at
2. 