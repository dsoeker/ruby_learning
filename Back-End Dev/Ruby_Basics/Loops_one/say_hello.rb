# Modify the code below so that you say "Hello!" a total of 5 times.

# Modify the code below so "Hello!" is printed 5 times.

=begin
say_hello = true

while say_hello
  puts 'Hello!'
  say_hello = false
end
=end
say_hello = 1

while say_hello
  puts 'Hello!'
  break if say_hello == 5
  say_hello += 1
end
