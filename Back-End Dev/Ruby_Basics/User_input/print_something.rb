# Part 1

# Write a program that asks the user whether they want the program to print "something", then print it if the user enters y. Otherwise, print nothing.

# Examples:

# $ ruby something.rb
# >> Do you want me to print something? (y/n)
# y
# something

# $ ruby something.rb
# >> Do you want me to print something? (y/n)
# n

# $ ruby something.rb
# >> Do you want me to print something? (y/n)
# help

=begin
def prompt(message)
  puts ">> #{message}"
end

prompt "Do you want me to print something? (y/n)"
answer = gets.chomp.downcase
puts 'something' if %w(y).include?(answer)
=end

# To validate the user's entry, we can change the case of the response.

# Part 2

# In the previous exercise, you wrote a program that asks the user if they want the program to print "something". However, this program recognized any input as valid: if you answered anything but y, it treated it as an n response, and quit without printing anything.

# Modify your program so it prints an error message for any inputs that aren't y or n, and then try again. In addition, your program should allow both Y and N (uppercase) responses; case sensitive input is generally a poor user interface choice. Whenever possible, accept both uppercase and lowercase inputs.

def prompt(message)
  puts ">> #{message}"
end

loop do
  prompt "Do you want me to print something? (y/n)"
  answer = gets.chomp.downcase
  if %w(y).include?(answer)
    puts 'something'
    break
  elsif %w(n).include?(answer)
    break
  else
    puts 'Invalid Entry, try again.'
  end
end

choice = nil
loop do
  puts '>> Do you want me to print something? (y/n)'
  choice = gets.chomp.downcase
  break if %w(y n).include?(choice)
  puts '>> Invalid input! Please enter y or n'
end
puts 'something' if choice == 'y'