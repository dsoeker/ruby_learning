# Write a program that asks the user for their age in years, and then converts that age to months.

# Examples:

# $ ruby age.rb
# >> What is your age in years?
# 35
# You are 420 months old.

def prompt(message)
  puts ">> #{message}"
end

def convert_to_months(answer)
  puts answer * 12
end

puts 'What is your age in years?'
answer = gets.chomp.to_i
convert_to_months(answer)

# What happens if you enter a non-numeric value for the age?

# The program outputs 0 since the answer from the user does not contain any numeric values. This would result in the convert_to_months method performing 12 * 0 = 0.