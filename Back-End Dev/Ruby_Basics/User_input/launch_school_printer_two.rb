# Modify this program so it repeats itself after each input/print iteration, asking for a new number each time through. The program should keep running until the user enters q or Q.

loop do
  puts '>> How many output lines do you want? Enter a number >= 3:'
  answer = gets.chomp.downcase
  break if answer == 'q'
  integer_answer = answer.to_i
  if integer_answer >= 3
    integer_answer.times { puts 'Launch School is the best!' } 
    break
  else
    puts '>> That''s not enough lines.'
  end
end