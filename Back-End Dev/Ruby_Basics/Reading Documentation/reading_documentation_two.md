# 1. How would you use String#upcase to create an uppercase version of the string "xyz"?

# Answer: "xyz".upcase

----
# 2. Assume you have this Array:

```a = %w(a b c d e)```

How would you use Array#insert to insert the numbers 5, 6, and 7 between the elements with values 'c' and 'd'?

# Answer: ```a.insert(3, 5, 6, 7)``` The three is the starting index value and everthing after will be inserted before the value at index 3.

----
# 3. Assume you have the following code:

```
s = 'abc def ghi,jkl mno pqr,stu vwx yz'
puts s.split.inspect
puts s.split(',').inspect
puts s.split(',', 2).inspect
```

What will each of the 3 puts statements print?

# Answer:
1. ```["abc", "def", "ghi,jkl", "mno", "pqr,stu", "vwx", "yz"]``` Splits the string where whitespace appears

2. ```["abc def ghi", "jkl mno pqr", "stu vwx yz"]``` Splits where each of the commas are located

3. ```["abc", "def ghi,jkl mno pqr,stu vwx yz"]``` Splits based on the comma delimiter, but into an array of size 2.

----
4. Assume you have the following code:

```
require 'date'

puts Date.new
puts Date.new(2016)
puts Date.new(2016, 5)
puts Date.new(2016, 5, 13)
```

What will each of the 4 puts statements print?

# Answer:
```Date.new```               everything defaults
```Date.new(2016)```         month, mday, start use defaults
```Date.new(2016, 5)```      mday, start use defaults
```Date.new(2016, 5, 13)```  start uses default

----
5. The ```Array#bsearch``` method is used to search ordered Arrays more quickly than #find and #select can. Assume you have the following code:

```a = [1, 4, 8, 11, 15, 19]```

How would you search this Array to find the first element whose value exceeds 8?