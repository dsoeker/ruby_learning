# 1. Where to Find Documentation
### Where can you find the most complete Ruby documentation?

Answer: http://ruby-doc.org/

----
# 2. While Loops
### Locate the description of the ```while``` loop in the ruby documentation.

Answer: http://ruby-doc.org/core-2.3.1/doc/syntax/control_expressions_rdoc.html

----
# 3. Return value of While
### Using the ruby documentation, determine what value a while loop returns.

Answer: The return value is ```nil``` unless ```break``` is used to supply a ```return``` value.

----
# 4. Return value of Break
### In the previous exercise, you learned that the ```while``` loop returns ```nil``` unless ```break``` is used. Locate the documentation for ```break```, and determine what value ```break``` sets the return value to for the ```while``` loop.

Answer: ```break``` accepts a value that supplies the result of the expression it is "breaking" out of. If there is no value being returned by a statement, then ```break``` returns ```nil```.

----
# 5. Large Numbers
### Using the ruby documentation, determine how you can write large numbers in a way that makes them easier to read.

Answer: Use Literals. http://ruby-doc.org/core-2.3.1/doc/syntax/literals_rdoc.html

----
# 6. Symbol Syntax
### Using the ruby documentation, determine how you would write a Symbol that represents your name. We aren't looking for a String; we want a Symbol, which is one of ruby's datatypes.

Answer: :Dustin
This is a way to create a variable that can take up one space in memory and still be used in multiple places. The idea is one object id vs many.

----
# 7. Default Arguments in the middle
### Consider the following method and a call to that method:

```
def my_method(a, b = 2, c = 3, d)
  p [a, b, c, d]
end

my_method(4, 5, 6)
```
### Use the ruby documentation to determine what this code will print.

Answer: [4, 5, 3, 6]. a = 4, b = the default value of 2 is replaced by 5, c = the default value of 3, d = 6.

----
# 8. String Class
### Lets move on now to the documentation you will use most often; the core API section on Classes and Modules. All of these are listed under Classes on the Core API page.

### Locate and open the class documentation for the String class.

Answer: http://ruby-doc.org/core-2.3.1/String.html

----
# 9. Right Justfying Strings
### Use the ruby documentation for the String class to determine which method can be used to right justify a String object.

Answer: String#rjsut

----
# 10. Class and Instance Methods
### Locate the ruby documentation for methods File::path and File#path. How are they different?

Answer: Class methods are called on the class, while instance methods are called on objects. Thus:

```puts File.path('bin')```
calls the class method File::path since we're calling it on the File class, while:

```
f = File.new('my-file.txt')
puts f.path
```

calls the instance method File#path since we're calling it on an object of the File class, namely f.
