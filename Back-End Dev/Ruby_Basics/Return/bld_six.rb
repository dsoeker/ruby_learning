# What will the following code print? Why? Don't run it until you've attempted to answer.

def meal
  return 'Breakfast'
  'Dinner'
  puts 'Dinner'
end

puts meal

# Will return 'Breakfast' and print nothing since the adding the return keyword will also cause the method to immediately exit.


