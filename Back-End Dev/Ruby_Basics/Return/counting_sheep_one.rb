# What will the following code print? Why? Don't run it until you've attempted to answer.

def count_sheep
  2.times do |sheep|
    puts sheep
  end
end

puts count_sheep

# Will return integers 0 - 5 since the times loop will iterate over a block int number of times. Eventhough the times method begins counting from 0, it will ultimately end with the original number since it returns this value at the end.

