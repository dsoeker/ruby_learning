# What will the following code print? Why? Don't run it until you've attempted to answer.

def count_sheep
  5.times do |sheep|
    puts sheep
  end
  10
end

puts count_sheep

# This will overwrite the return of 5 at the end of the loop and replace it with 10 as the new return value since it is the last line of the method.