# Use Hash#map to iterate over numbers and return an array containing each number divided by 2. Assign the returned array to a variable named half_numbers and print its value using #p.

numbers = {
  high:   100,
  medium: 50,
  low:    10
}

# Expected output:

# [50, 25, 5]

num_array = numbers.map do |k, v|
              v / 2
            end

p num_array

# Hash#map works similarly to Array#map except for two notable differences. First, Hash#map has two block parameters, instead of one, to account for both the key and the value. Second, you might expect Hash#map to return a Hash, but it actually returns an Array. This is because #map creates a new Array based on the return value of the block regardless of the data structure it is used on.