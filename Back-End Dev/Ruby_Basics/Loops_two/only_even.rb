# Using next, modify the code below so that it only prints even numbers.

# number = 0

# until number == 10
  # number += 1
  # puts number
# end

number = 0

until number == 10
  next if number.odd?
  number += 1
  puts number
end

# The next keyword will ignore everything remaining in the block and move back to the top of the loop.

# Why did next have to be placed after the incrementation of number and before #puts?
  # If next was placed above the incrementer, then the loop would stall at 1 and continue between the first two lines of the until block ifinitely.
  