=begin
Write a method named print_me that prints "I'm printing within the method!" when invoked.

print_me

def print_me
  puts "I'm printing within the method!"
end

print_me

Write a method named print_me that prints "I'm printing the return value!" when using the following code.
=end

def print_me
  "I'm printing the return value!"
end

puts print_me