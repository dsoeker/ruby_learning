## Local Variables

**Question 1**. In the code below, explain why the last line outputs ```2```. What's the underlying principle this example illustrates?

```ruby
str = 1

loop do
  str = 2
  break
end

puts str
```

**Answer:**
In the above example, outputting ```2``` shows the use of local variable scope. Since the ```str``` variable was initialized outside the ```loop do``` block, we were able to use the reassingment from ```1``` to ```2``` that took place inside the block.

---

**Question 2**. Explain why the code below raises an exception.

```ruby
loop do
  str = 2
  break
end

puts str
```

**Answer:**
The only area this program knows the ```str``` variable exists is inside the ```loop do``` block. Outer scope cannot access variables initialized in an inner scope.

---

**Question 3**. Suppose you can only see the code shown below, and there are lots of code above the shown snippet. Can you say with certainty whether the below code snippet will run? Why or why not?

```ruby
# .. lots of code up here omitted

loop do
  str = 2
  break
end

puts str
```

**Answer:**
This could be a question of inner scope variables not being accessible to the outer scope. If ```str``` has been initialized earlier in the program, then this piece of the program should be fine; if not, then an exception will be thrown. We might also have a situation where ```str``` could be a method that was defined earlier in the program, which would allow the program to run. However, in light of the aforementioned possibilities, it's difficult to be absolutely certain that this program will run.

----

**Question 4**. Explain why the following code raises an exception.

```ruby
str = "hello"

def a_method
  puts str
end

a_method
```

**Answer:**
The method ```a_method``` takes no parameters and there is no initialization of ```str``` within the inner scope of ```a_method``` in order to print the value of ```str```.

---

**Question 5**. Explain why the code below doesn't raise an exception, like it does in the previous example, and instead outputs ```"hello"```. What's the underlying principle in this code example? Why is there such a dramatic difference between the previous question and the code in this question? What's the relationship between the two str local variables -- are they the same variable, are they different, do they share the same scope?

```ruby
 str = "hello"

 def a_method
   str = "world"
 end

 a_method
 puts str
```

**Answer:**
In the previous example, ```a_method``` is being asked to output the value of ```str```; however, here were are assigning a new ```str``` variable with inner scope. The ```str = "hello"``` variable has outer scope, while ```str = "world"``` has inner scope. Thus, they are different and do not share the same scope.

---

**Question 6**. Explain why ```b``` is still ```"hello"```.

```ruby
a = "hello"
b = a 
a = "hi"
a << " world"

puts a
puts b
```

**Answer:**
After the declaration of ```a = b``` there is a reassingment of ```a``` to ```"hi"``` instead of ```"hello"```. Meanwhile, ```b``` retains the original assignment of ```"hello"``` since the reassignment comes before the destructive ```<<```.

---

**Question 7**. How many variables and how many objects are there in the code below? How did you come up with your answer?

```ruby
a = "hello"
b = "world"

c = a
d = b
b = a
a = c
```

**Answer:**
If you apply the ```object_id``` method to each variable, you find there are only 2 objects in this code. ```a```, ```b```, and ```c``` all share an object, while ```d``` is assigned to a seperate object. Meanwhile, there a 4 variables, ```a```, ```b```, ```c```, and ```d```. 

---------------------------------------------

## Mutating Method Arguments

---

**Question 1**. Explain why the last line in the below code prints ```"hello"```. What does this demonstrate?

```ruby
def change(param)
  param + " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Answer:**
With the ```change``` method we are aksing the assignment of ```param``` to concatenate ```" world"``` to the object ```hello```, which is now seperate from the object assigned to ```greeting```. This is a demonstration of pass-by-value.

---

**Question 2**. Explain why the last line in the below code prints "hello world". What does this demonstrate?

```ruby
def change(param)
  param << " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Answer:**
This is an example of mutating the caller. Using the ```<<``` operator inside the ```change``` method permenantely reassigns the value of the argument being passed into the method (pass by reference).

---

**Question 3**. We're going to modify the change method slightly. Why does the last line print ```"hello"``` now? Why did adding the one extra line of code in change method so dramatically affect the final output? Be very precise with how you explain what's happening here.

```ruby
def change(param)
  param = "hi"
  param << " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Answer:**
Within the ```change``` method the first line ```param = "hi"``` initializes and assigns the value of ```param``` to something other than what was passed into the method, now making this pass by value. The method turns it's focus to the new inner scope variable ```param``` instead of the argument, ```greeting```. Thus, we have the ```change``` method returning the value of ```param``` as ```"hi world"``` (since nothing is explicitly being asked to output to the screen) while leaving ```greeting``` as ```"hello"```.

---

**Question 4**. Explain why the last line outputs ```"hello"```. Be precise.
```ruby
def change(param)
  param += "greeting"
  param << "hey"
  param = "hi"
  param << " world"
end

greeting = "hello"
change(greeting)

puts greeting
```

**Answer:**
Within the ```change``` method;

  1. ```param += "greeting"``` > Takes the initial value passed and concatenates ```"greeting"```. Now. however, there's a new local variable in the method ```param``` assigned to value ```"hellogreeting"```.

  2. ```param << "hey"``` > Takes ```param = "hellogreeting"``` and concatenates ```"hey"``` making the new value, ```param = "hellogreetinghey"```.

  3. ```param = "hi"``` > Now the value of ```param``` has been reassigned to ```"hi"``` from ```"hellogreetinghey"```.

  4. ```param << "world"``` > This appends ```"world"``` to the value of ```param``` returning the final value of the local variable ```param``` as ```"hi world"```.

Meanwhile, the variable ```greeting``` is left alone turning this into a matter of pass by value - modifying ``` param```, rather than pass by reference - modifying ```greeting```.

---------------------------------------------

## Working with Collections

---

**Question 1**. Write down your definition of the ```Array#map``` method. Do not copy/paste from documentation; use your own words.

**Answer:** The ```Array#map``` method takes an object and a block and runs the block for each element returning each value into a new array.

---

**Question 2**. Write down your definition of the ```Array#select``` method. Do not copy/paste from documentation; use your own words.

**Answer:** The ```Array#select``` method will run a block on every element in the enumerable object and returns an array for which the block returns non-nil and non-false values.

---

**Question 3**. Explain why both of the code below return an array containing the same elements. Explain the subtle difference between the two lines of code. Which one is the preferred option and why?

```ruby
[1, 2, 3].map {|n| n + 1}

[1, 2, 3].map {|n| n += 1}
```

**Answer:**
It's true that both of the code blocks return the same ```[2, 3, 4]``` array and use the same arithmetic operator, ```+```. However, one ```n + 1 ``` is adding values on either side of the operator, while the other ```n += 1``` adds the left side of the operator to the right and assigns the result to the left. Since adding AND assigning a value to a temporary variable in the case of ```map``` seems unnecessary, ```n + 1``` seems more efficient.

---

**Question 4**. Some people don't fully understand the difference between ```map``` and ```select```, and use ```map``` as they would ```select```. However, they likely won't get the result they expected. Explain why the resulting ```arr``` is an array of booleans.

```ruby
arr = [1, 2, 3].map do |n|
  n > 2
end

p arr
```

**Answer:**
Within the code block, we are asking the ```map``` method to evaluate the collection based on a comparison operator. Using ```select``` would filter the array to show only elements that are greater than 2, while ```map``` will return a new array of boolean values.

---

**Question 5**. The above code is a little odd, but perfectly valid Ruby. Suppose the person who wrote this code is trying to figure out why they're getting an array of booleans, so they put a ```puts``` in the block (see code below). Now, what did ```arr``` change into and why? Be precise with your explanation.

```ruby
arr = [1, 2, 3].map do |n|
  n > 2
  puts n
end

p arr
```

**Answer:**
If the ```puts n``` line was removed, then the code block would return ```[false, false, true]``` since ```map``` is designed to return a new collection for each element in current object, whether true or false. However, since ```puts n``` is added, the block will still execute ```n > 2``` but will return the last line ```puts n``` instead. This will print each element of the current collection into a new object ```[1, 2, 3]``` and return ```[nil, nil, nil]```.

---

**Question 6**. Continuing with the misunderstanding of ```map``` and ```select```, suppose this person wrote the below code thinking that it should increment every element by 2. However, that's not how select works. What is ```arr``` and why? Be precise in your explanation and explain exactly how ```select``` works.

```ruby
arr = [1, 2, 3].select do |n|
  n + 2
end

p arr
```

**Answer:**
Using ```select``` to increment every element will return the original object becasue it's only meant to return values that are non-nil and non-false. Using ```map``` in this case would provide the desired incrementation and return ```[3, 4, 5]```.

---

**Question 7**. Using the code above, let's say this person adds a ```puts``` in the block to try to figure out what's going on. What's ```arr``` and why?

```ruby
arr = [1, 2, 3].select do |n|
  n + 2
  puts n
end

p arr
```

**Answer:**
The result will print the original object ```[1, 2, 3]``` assigned to ```arr``` and return an empty array. Outside of selecting and returning elements of a collection for which a block returns non-nil non-false values, the ```select``` method will not be much good. 