# Getting Started with HTML
-----------------------------
Acronym for Hyper Text Markup Language, the core language of the web.

The part of the site that puts all of the content into their own respective compartments and boxes is HTML. The primary use is to provide meaning to the different types of content on the page, and provide the web browser with a quick term that tells it how to display the content being provided.

Element = The basic building block of HTML. Consists of an element name and angle bracks "< >"

The HTML Element
-------------------


** See the html_example_one.html file for examples.
