# Styling with CSS
--------------------

Cascading Style Sheets are what is used to provide web browsers with instructions on how to present everything visually. While HTML is used to define the types of content, CSS (the acronym for Cascading Style Sheets) is the technology used to apply spacing, colors, font styles, and even background images. Just about everything on a web page can have its presentation modified through CSS.

CSS Property
-----------------
The way we write our style instructions is through the use of property and value pairs. CSS properties tell the browser which aspect of the element's design to change with the supplied value. Here's an example CSS property:

color: red;

As you can imagine, this will set the text color within an element to red. The color portion is the property, and red is the value. The colon separates the two, and the semicolon designates the end of that style rule.

Style Attribute
-----------------
There are a few HTML attributes that are universally available, meaning they can be added to any HTML element. The style attribute is one of these. This attribute can hold any number of CSS properties and their values. Let's go back to our h1 element and add a text color to it using the style attribute:

<h1 style="color: orange;">Hello Internet!</h1>

We also want to change the font size that is being used on our paragraph:

<p style="font-size: 18px;">

Font-size Property
---------------------
This allows us to change the font size of one or more elements, using a measurement known as pixels. The above example adjusts the height to that each line takes up to be 18 pixels tall. Color and font-size are both inheritable, but as you'll find as we hear about more properties the majority of them are not. In cases where they are not inheritable, it's because it's unlikely the desired effect is to also have those attributes applied to all child elements. With text-manipulation properties, you likely do want to inherit them.

Text-align Property
---------------------
Since we're only going to affect the text of the element, this starts to make more sense. When you use a word processing program, the text alignment can be adjusted to be left, right, or center aligned. The same can be done with this property.

The style attribute only affects the element it's on, not all of that type of element. This is why CSS became a separate language. In the past, you changed how things looked with attributes and other elements meant for styling rather than assigning meaning. With those methods came tedious repetition of HTML. Imagine a list of 100 items that each have to have the same style attribute containing the same 5 CSS properties. That's a lot of code to deal with. The solution was to separate the design and semantic concerns into CSS and HTML, respectively.

Style Element
-----------------
The style element is one way to further separate our design from our content and allow for the reuse of both single properties and whole blocks. Since the contents of the style element shouldn't be visible to the user, this generally goes in the head element on our page. The style element looks like this:

<style type="text/css">

Modern browsers now assume that the style element contains CSS, so the attribute can be omitted.

Give the style element a try. Create a style element and move the style properties from the h1 to the style element. This doesn't do anything. Our h1 is back to the default style, which means that the browser is ignoring this. So, how do we tell the browser that the properties belong to the h1 element? CSS Selector.

CSS Selector
---------------
This is a special notation for targeting one or more HTML elements by either the element name or some attribute on the element. You can also combine those selector types into one more specific selector in order to refine what elements you are targeting.

The most basic selector is called the element selector. This is simply the name of the HTML element, without the angle brackets that HTML uses. After you specify your selector, the CSS properties and values are enclosed in a kind of code fence using curly braces, { and }. These let the browser know where the block starts and ends. For our h1, we would write our CSS properties in an element selector and CSS block like this:

h1 {
  color: orange;
  text-align: center;
}

Because the browser has colons, semicolons, and curly braces to let it know where everything starts and ends, you can format it how you like.

What if we only wanted to add styles to one paragraph on the page? If this paragraph is our intro paragraph, for example, and is the only one that should have a larger font size, how do we prevent other paragraphs from receiving this style? Enter: ID and Class Selectors.

ID and Class Selectors
-------------------------
We have other types of CSS selectors we can use to refine our selection of elements. The two most common methods of filtering the elements you're looking to target use optional global attributes you can add to your elements. The two attributes, class and ID, can both be placed on the same element but the ID element has to be unique to all of the ID values on the page. Any number of elements can have the same class name, but ID names must be unique. The class attribute can also contain multiple names, allowing you to apply multiple sets of styles to the same element and providing you with a means to have pre-made building blocks of styles to apply to elements.

Add the attribute to the element:

<p id="intro">

Now we can change our selector to an ID selector. ID selectors use the value you've given to the HTML element's ID attribute with a # sign before it. The ID selector also takes greater precedence than an element selector, so if you had two blocks of style like this:

p {
  font-size: 14px;
}

#intro {
  font-size: 18px;
}

The intro selector will override the 'p' element selector for our intro paragraph. Even if we changed the order these two selectors appear, the ID attribute property will be used. This is where the cascade part of the name cascading style sheets comes in to play. As the browser reads the CSS properties, it assigns the values. If it encounters an override property that has the same or greater weight than the previous selector that set it, the new property will be used.

The class attribute and selector work the same way. If you added a class of highlight, for example, to an element and wanted to create a style that would apply a light yellow background to the text, you could add the class name to an element with the attribute:

<p>Welcome to my website! It is currently a work in progress as I learn <strong class="highlight">HTML</strong> and <strong>CSS</strong> from this <em>excellent</em> book I'm learning from.</p>

Then use the class selector in your CSS styles. The class selector is like the ID selector, but the # character is replaced with a period (.). Use this class selector and give it the background color shown below.

.highlight {
  background-color: #f0f099;
}

Hex Colors
-------------
When you want to fine-tune the colors beyond the color keywords like red and blue, you would use a measurement like hex to tell the browser how much red, green, and blue to use to create the color.

This hex value can be viewed as a # sign to designate hex measurement, then 3 pairs of two digits to set red, green, and blue values. Hexadecimal, if you're unfamiliar, has 16 digits per digit place instead of our decimal number system. After the value 9, values A through F are used to represent the numbers 10 through 15.

Now that we have a class for our highlight color, we can add it to multiple elements. Add it to the other strong element as well as the em element.

<p>Welcome to my website! It is currently a work in progress as I learn <strong class="highlight">HTML</strong> and <strong class="highlight">CSS</strong> from this <em class="highlight">excellent</em> book I'm learning from.</p>

Our style is applied regardless of what element type it is. If we wanted to restrict it to just strong elements, we could combine our element and class selectors to make the selector more specific:

strong.highlight {
  background-color: #f0f099;
}

How about adding classes for three base text colors? Add class names for red, green, and blue, and set the color property to the respective color:

.red {
  color: red;
}

.green {
  color: green;
}

.blue {
  color: blue;
}

Add one of each of the classes to the three highlighted elements by adding the class name to the class attribute and leave a space between it and the highlight class:

<p>Welcome to my website! It is currently a work in progress as I learn <strong class="highlight red">HTML</strong> and <strong class="highlight green">CSS</strong> from this <em class="highlight blue">excellent</em> book I'm learning from.</p>
